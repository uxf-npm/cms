// eslint-disable-next-line @typescript-eslint/no-var-requires
const express = require("express");
// eslint-disable-next-line @typescript-eslint/no-var-requires
const next = require("next");
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { createProxyMiddleware: proxy } = require("http-proxy-middleware");

const app = next({
    dev: process.env.NODE_ENV !== "production",
});

app.prepare()
    .then(() => {
        express()
            .use(/^\/(api|generated|upload|assets)/, proxy(process.env.API_URL, { changeOrigin: true }))
            .use(app.getRequestHandler())
            .listen(3000, () => {
                console.log(`> Listen on port 3000`); // eslint-disable-line no-console
                console.log(`> http://localhost:3000`); // eslint-disable-line no-console
            });
    })
    .catch((ex) => {
        console.error(ex.stack); // eslint-disable-line no-console
        process.exit(1);
    });
