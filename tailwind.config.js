/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./node_modules/@uxf/data-grid/tailwindui/**/*.js", "./src/**/*.tsx"],
    theme: {
        extend: {
            colors: {
                dataGridPrimary: "#140939",
                dataGridSecondary: "#FF4F06",
                primary: "#140939",
                secondary: "#FF4F06",
            },
        },
    },
    plugins: [require("@tailwindcss/forms")({ strategy: "class" })],
};
