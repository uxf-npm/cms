import { Typography } from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import axios from "axios";
import { NextPage } from "next";
import React, { ReactNode, useState } from "react";
import { menu } from "../config";
import { Layout } from "../src/lib/Layout";
import { FlashMessagesService } from "../src/services/FlashMessagesService";
import { Autocomplete } from "../src/ui/Autocomplete";
import { AutocompleteMultiple } from "../src/ui/AutocompleteMultiple";
import { CopyToClipboard } from "../src/ui/CopyToClipboard";
import { CopyToClipboardButton } from "../src/ui/CopyToClipboardButton";
import { FileInput } from "../src/ui/FileInput";
import { Label, LabelProps } from "../src/ui/Label";
import { Paper } from "../src/ui/Paper";
import { Select } from "../src/ui/Select";
import { DatePicker } from "../src/ui/DatePicker";
import { FileResponse, FileUploadInput } from "../src/ui/FileUploadInput";

const labelColors: Array<LabelProps["color"]> = ["primary", "secondary", "success", "warning", "error", "disabled"];

type Option = { id: number; label: string };

const smartAddressAxios = axios.create({
    baseURL: "https://www.smart-address.cz",
});

const loader = async (term: string): Promise<Array<Option>> => {
    const response = await smartAddressAxios.get("/api/v1/search", { params: { term, level: 1 } });
    return response.data.map((a: any) => ({ id: a.id, label: `${a.ulice}, ${a.obec}` }));
};

const options = [
    { id: 1, label: "Option 1" },
    { id: 2, label: "Option 2" },
    { id: 3, label: "Option 3" },
];

const Wrapper: React.FC<{ title: string; children: ReactNode }> = (props) => {
    const { title, children } = props;

    return (
        <Paper mb={2}>
            <Typography component="h2" variant="h4">
                {title}
            </Typography>
            <Box mt={2}>{children}</Box>
        </Paper>
    );
};

const Index: NextPage = () => {
    const [autocompleteValue, setAutocompleteValue] = useState<Option | null>(null);
    const [autocompleteMultipleValue, setAutocompleteMultipleValue] = useState<Option[]>([]);
    const [datePickerValue, setDatePickerValue] = useState<any>(null);
    const [fileInputValue, setFileInputValue] = useState<File | null | undefined>(null);
    const [fileUploadInputValue, setFileUploadInputValue] = useState<FileResponse | null | undefined>(null);

    return (
        <Layout title="UI komponenty" subtitle="Subtitle" menuConfiguration={menu} header={<>Header obsah</>}>
            <Wrapper title="Autocomplete">
                <Autocomplete
                    label="Autocomplete"
                    loadOptions={loader}
                    value={autocompleteValue}
                    onChange={(_, value) => setAutocompleteValue(value)}
                />
            </Wrapper>
            <Wrapper title="AutocompleteMultiple">
                <AutocompleteMultiple
                    label="Autocomplete multiple"
                    loadOptions={loader}
                    value={autocompleteMultipleValue}
                    onChange={(_, value) => setAutocompleteMultipleValue(value)}
                    getOptionLabel={(o) => o.label}
                />
            </Wrapper>
            <Wrapper title="CopyToClipboard">
                <CopyToClipboard value="https://www.uxf.cz" label="Zkopírovat url adresu" />
            </Wrapper>
            <Wrapper title="ClipboardButton">
                <CopyToClipboardButton textToCopy="Text ke zkopírování" />
            </Wrapper>
            <Wrapper title="DatePicker">
                <DatePicker label="DatePicker" value={datePickerValue} onChange={setDatePickerValue} clearable />
            </Wrapper>
            <Wrapper title="FileInput">
                <FileInput
                    id="file-input-name"
                    name="file-input"
                    value={fileInputValue}
                    onChange={setFileInputValue}
                    label="Profilová fotka"
                    placeholder="Vyberte soubor"
                />
            </Wrapper>
            <Wrapper title="FileUploadInput">
                <FileUploadInput
                    id="file-upload-input-name"
                    name="file-upload-input"
                    value={fileUploadInputValue}
                    onChange={setFileUploadInputValue}
                    label="Profilová fotka"
                    placeholder="Vyberte soubor"
                />
            </Wrapper>
            <Wrapper title="FlashMessage">
                <Button onClick={() => FlashMessagesService.info("Info!")} variant="outlined">
                    Info
                </Button>
                <Button
                    onClick={() => FlashMessagesService.success("Success!")}
                    variant="outlined"
                    color="success"
                    sx={{ ml: 2 }}
                >
                    Success
                </Button>
                <Button
                    onClick={() => FlashMessagesService.warning("Warning!")}
                    variant="outlined"
                    color="warning"
                    sx={{ ml: 2 }}
                >
                    Warning
                </Button>
                <Button
                    onClick={() => FlashMessagesService.error("Error!")}
                    variant="outlined"
                    color="error"
                    sx={{ ml: 2 }}
                >
                    Error
                </Button>
            </Wrapper>
            <Wrapper title="Label">
                <Box>
                    {labelColors.map((c) => (
                        <Label key={c} color={c} mr={2} mt={2}>
                            {c}
                        </Label>
                    ))}
                </Box>
                <Box>
                    {labelColors.map((c) => (
                        <Label key={c} color={c} light mr={2} mt={2}>
                            {c}
                        </Label>
                    ))}
                </Box>
                <Box>
                    {labelColors.map((c) => (
                        <Label key={c} color={c} mr={2} mt={2} size="small">
                            {c}
                        </Label>
                    ))}
                </Box>
                <Box>
                    {labelColors.map((c) => (
                        <Label key={c} color={c} light mr={2} mt={2} size="small">
                            {c}
                        </Label>
                    ))}
                </Box>
            </Wrapper>
            <Wrapper title="Select">
                <Select<any> label="Autocomplete" onChange={() => null} value={null} options={options} />
            </Wrapper>
        </Layout>
    );
};

export default Index;
