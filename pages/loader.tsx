import { NextPage } from "next";
import { menu } from "../config";
import { Layout } from "../src/lib/Layout";

const Loader: NextPage = () => <Layout menuConfiguration={menu} loading />;

export default Loader;
