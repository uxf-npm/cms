import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Divider from "@mui/material/Divider";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import arrayMutators from "final-form-arrays";
import { NextPage } from "next";
import React from "react";
import { Form } from "react-final-form";
import { menu } from "../config";
import { Autocomplete, AutocompleteProps } from "../src/forms/components/Autocomplete";
import { AutocompleteMultiple } from "../src/forms/components/AutocompleteMultiple";
import { Checkbox } from "../src/forms/components/Checkbox";
import { DatePicker } from "../src/forms/components/DatePicker";
import { DateTimePicker } from "../src/forms/components/DateTimePicker";
import { DropZone } from "../src/forms/components/DropZone";
import { FileUploadInput } from "../src/forms/components/FileUploadInput";
import { GpsInput } from "../src/forms/components/GpsInput";
import { NumberInput } from "../src/forms/components/NumberInput";
import { RadioGroup } from "../src/forms/components/RadioGroup";
import { Select } from "../src/forms/components/Select";
import { TextInput } from "../src/forms/components/TextInput";
import { Layout } from "../src/lib/Layout";
import { ContentComponent, ContentField } from "../src/pages/ContentBuilder";
import { FeatureSection } from "../src/pages/ContentBuilder/content/FeatureSection";
import { Gallery } from "../src/pages/ContentBuilder/content/Gallery";
import { HeroSection } from "../src/pages/ContentBuilder/content/HeroSection";
import { People } from "../src/pages/ContentBuilder/content/People";
import { Wysiwyg } from "../src/pages/ContentBuilder/content/Wysiwyg";
import { Paper } from "../src/ui/Paper";

const loadOptions: AutocompleteProps<any, { id: number; label: string }>["loadOptions"] = async function (
    inputValue: string,
) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(
                [
                    { id: 1, label: "Czech Republic" },
                    { id: 2, label: "Austria" },
                    { id: 2, label: "Australia" },
                    { id: 3, label: "Germany" },
                    { id: 4, label: "Switzerland" },
                ].filter((item) => item.label.indexOf(inputValue) > -1),
            );
        }, 1000);
    });
};

const SELECT_OPTIONS = [
    { id: 1, label: "Položka 1" },
    { id: 2, label: "Položka 2" },
];

const CONTENT_COMPONENTS: ContentComponent<any>[] = [Wysiwyg, People, Gallery, HeroSection, FeatureSection];

// TODO @vejvis - tady nefunguje levé menu (pokud tam je tak se rozbije dizajn)
const FormPage: NextPage = () => {
    return (
        <Layout title="Formulářové komponenty" menuConfiguration={menu}>
            <Paper ph={2} mt={2} ml={2} mr={2}>
                <Form onSubmit={console.log} mutators={{ ...arrayMutators }}>
                    {({ handleSubmit, values }) => {
                        return (
                            <Grid container spacing={2}>
                                <Grid item xs={6}>
                                    <form onSubmit={handleSubmit}>
                                        <Grid container spacing={2}>
                                            <Grid item xs={12}>
                                                <Divider />
                                                <Box sx={{ p: 2 }}>
                                                    <Typography fontWeight="bold">ContentField</Typography>
                                                    <ContentField
                                                        contentComponents={CONTENT_COMPONENTS}
                                                        name="content"
                                                    />
                                                </Box>
                                                <Divider />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <DropZone name="files" />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <DatePicker name="date-picker" label="Vyberte datum" required />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <DateTimePicker
                                                    name="date-time-picker"
                                                    label="Vyberte datum a čas"
                                                    required
                                                />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <Autocomplete
                                                    name="autocomplete"
                                                    label="Autocomplete"
                                                    loadOptions={loadOptions}
                                                    getOptionLabel={(o) => o.label}
                                                    required
                                                />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <AutocompleteMultiple
                                                    name="autocompleteMultiple"
                                                    label="AutocompleteMultiple"
                                                    loadOptions={loadOptions}
                                                    required
                                                />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <Select
                                                    name="select"
                                                    label="Select"
                                                    options={SELECT_OPTIONS}
                                                    required
                                                />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <FileUploadInput
                                                    id="file-upload-input"
                                                    name="fileUploadInput"
                                                    label="Profilová fotografie *"
                                                    placeholder="Vyberte fotku"
                                                    required
                                                />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <Checkbox name="checkbox" label="Checkbox" />
                                                <Checkbox
                                                    name="checkboxWithHelperText"
                                                    label="Checkbox"
                                                    helperText="Checbox s helper textem."
                                                />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <TextInput name="text.input" label="TextInput" />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <TextInput
                                                    name="text.inputHelperText"
                                                    label="TextInput"
                                                    required
                                                    helperText="Text input s helper textem"
                                                />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <TextInput
                                                    name="text.inputMultiline"
                                                    label="TextInput multiline"
                                                    required
                                                    multiline
                                                />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <NumberInput
                                                    name="integer-input"
                                                    label="Integer input"
                                                    decimalPlaces={0}
                                                    required
                                                />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <NumberInput name="float-input" label="Float input" required />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <NumberInput
                                                    name="float-input-3"
                                                    label="Float input (3 decimal place)"
                                                    required
                                                    decimalPlaces={3}
                                                />
                                            </Grid>
                                            <Grid xs={4} item>
                                                <RadioGroup
                                                    name="radio-1"
                                                    label="Simple radio group"
                                                    required
                                                    options={[
                                                        { id: 1, label: "A" },
                                                        { id: 2, label: "B" },
                                                    ]}
                                                />
                                            </Grid>
                                            <Grid xs={4} item>
                                                <RadioGroup
                                                    name="radio-2"
                                                    label="Simple radio group with helper text"
                                                    required
                                                    options={[
                                                        { id: 1, label: "A" },
                                                        { id: 2, label: "B" },
                                                    ]}
                                                    helperText="Napiš něco"
                                                />
                                            </Grid>
                                            <Grid xs={4} item>
                                                <RadioGroup
                                                    name="radio-3"
                                                    label="Simple radio horizonal"
                                                    required
                                                    options={[
                                                        { id: 1, label: "A" },
                                                        { id: 2, label: "B" },
                                                    ]}
                                                    row
                                                />
                                            </Grid>
                                            <Grid xs={12} item>
                                                <GpsInput name="gps" label="GPS input" required />
                                            </Grid>
                                            <Grid xs={12} item px={2}>
                                                <Button type="submit" variant="contained" color="primary">
                                                    Save
                                                </Button>
                                            </Grid>
                                        </Grid>
                                    </form>
                                </Grid>
                                <Grid item xs={6}>
                                    <pre
                                        style={{
                                            backgroundColor: "#f5f5f5",
                                            padding: 8,
                                            borderRadius: 4,
                                            marginTop: 0,
                                            // height: 300,
                                            // overflowY: "scroll",
                                        }}
                                    >
                                        {JSON.stringify(values, null, "    ")}
                                    </pre>
                                </Grid>
                            </Grid>
                        );
                    }}
                </Form>
            </Paper>
        </Layout>
    );
};

export default FormPage;
