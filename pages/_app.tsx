import { ThemeProvider, StyledEngineProvider } from "@mui/material/styles";
import App, { AppProps } from "next/app";
import "dayjs/locale/cs";
import React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import { useSelector } from "react-redux";
import { getLoggedUser } from "../src/deprecated/redux/user";
import { route } from "../routes";
import { config, container } from "../src/config";
import { withReduxStore } from "../src/deprecated/redux";
import { UnauthorizedError } from "../src/errors/UnauthorizedError";
import { CmsProvider } from "../src/lib/CmsProvider";
import { FlashMessagesContainerInstance } from "../src/lib/FlashMessages";
import { theme } from "../src/styles/theme";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { CacheProvider, EmotionCache } from "@emotion/react";
import createEmotionCache from "../src/utils/createEmotionCache";
import { locale } from "dayjs";
import "../src/styles/globals.css";
import "@uxf/data-grid/tailwindui/styles.css";

const clientSideEmotionCache = createEmotionCache();

config.set("api-url", "http://localhost:3000");

container
    .register("service.error", {
        handleError: console.log,
        logError: console.log,
    })
    .register("route", route as any)
    .register("useLoggedUser", () => useSelector(getLoggedUser) ?? { roles: [] }); // TODO

interface Props extends AppProps {
    emotionCache?: EmotionCache;
}

function CmsApp(props: Props) {
    locale("cs");

    const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

    React.useEffect(() => {
        // Remove the server-side injected CSS.
        const jssStyles = document.querySelector("#jss-server-side");
        if (jssStyles && jssStyles.parentElement) {
            jssStyles.parentElement.removeChild(jssStyles);
        }
    }, []);

    return (
        <CacheProvider value={emotionCache}>
            <StyledEngineProvider injectFirst>
                <ThemeProvider theme={theme}>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <CmsProvider>
                            <CssBaseline />
                            <Component {...pageProps} />
                            <FlashMessagesContainerInstance />
                        </CmsProvider>
                    </LocalizationProvider>
                </ThemeProvider>
            </StyledEngineProvider>
        </CacheProvider>
    );
}

CmsApp.getInitialProps = async (appContext: any) => {
    const ctx = appContext.ctx;
    try {
        const appProps = await App.getInitialProps(appContext);
        return { ...appProps } as any;
    } catch (e) {
        if (e instanceof UnauthorizedError) {
            if (typeof window === "undefined") {
                if (ctx.res && !ctx.res.headersSent) {
                    ctx.res.writeHead(302, { Location: `/pages/login?redirect=${ctx.pathname}` });
                    ctx.res.end();
                }
            } else {
                window.location.href = `/pages/login?redirect=${ctx.pathname}`;
            }
            return {};
        }

        throw e;
    }
};

export default withReduxStore(CmsApp);
