import { menu } from "../../config";
import { Layout } from "../../src/lib/Layout";
import { ContentBuilderPage } from "../../src/pages/ContentBuilder";
import { FeatureSection } from "../../src/pages/ContentBuilder/content/FeatureSection";
import { Gallery } from "../../src/pages/ContentBuilder/content/Gallery";
import { HeroSection } from "../../src/pages/ContentBuilder/content/HeroSection";
import { People } from "../../src/pages/ContentBuilder/content/People";
import { Wysiwyg, WysiwygContent } from "../../src/pages/ContentBuilder/content/Wysiwyg";

type Contents = WysiwygContent;

export default ContentBuilderPage<Contents>({
    contentComponents: [Wysiwyg, People, Gallery, HeroSection, FeatureSection],
    allowedTypes: [
        { id: "BLOG", label: "Článek" },
        { id: "LANDING_PAGE", label: "Landing page" },
        { id: "QUESTION", label: "Otázka a odpověď" },
        { id: "TERM", label: "Pojem" },
    ],
    ui: {
        Layout: (props) => (
            <Layout menuConfiguration={menu} title="Nový článek">
                {props.children}
            </Layout>
        ),
    },
});
