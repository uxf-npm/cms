import { BaseField, FormPage } from "../../../src/pages/FormPage";
import { Layout } from "../../../src/lib/Layout";
import { menu } from "../../../config";
import { FlashMessagesService } from "../../../src/services/FlashMessagesService";
import { Content } from "../../../src/pages/FormPage/Field/Content";
import { Wysiwyg } from "../../../src/pages/ContentBuilder/content/Wysiwyg";

const WysiwygInstance = Wysiwyg.create({});

export default FormPage<any>({
    entityAlias: "user",
    entityId: 1,
    onSaveDone: () => {
        FlashMessagesService.success("Uloženo");
    },
    ui: {
        Layout: <Layout title="FormPage" menuConfiguration={menu} />,
        Field: (props) => {
            switch (props.fieldSchema.type) {
                case "content":
                    return <Content {...props} contentComponents={[WysiwygInstance]} />;
                default:
                    return <BaseField {...props} />;
            }
        },
    },
});
