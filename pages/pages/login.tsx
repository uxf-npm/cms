import Typography from "@mui/material/Typography";
import Router from "next/router";
import { LoginPage } from "../../src/pages/LoginPage";

const Page = LoginPage({
    pageTitle: "Přihlášení | UXFans",
    title: "Přihlášení",
    onLoginDone: (_, redirectUrl) => {
        Router.push({ href: redirectUrl });
        return Promise.resolve();
    },
    onForgottenPassword: () => console.log("Forgotten password."),
    loggedUserRedirectUrl: "/",
    ui: {
        hideTitle: true,
        Logo: <Typography>Logo</Typography>,
    },
});

export default Page;
