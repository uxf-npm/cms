import { menu } from "../../config";
import { Layout } from "../../src/lib/Layout";
import { GridPage } from "../../src/pages/GridPage";

export default GridPage({
    entityAlias: "user",
    ui: {
        Layout: (props) => <Layout {...props} menuConfiguration={menu} />,
    },
    onEdit: (entityAlias) => ({ entityAlias }),
    getOpenUrl: (_, row) => `https://www.uxf.cz/${row.id}`,
    onAdd: console.log,
});
