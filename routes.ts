import { createRouter } from "@uxf/router";

export interface RouteList {
    index: null;
    form: null;
    loader: null;
    "forms/user-invitation": null;
    "pages/form-page": { id: string };
    "pages/forgotten-password-page": null;
    "pages/login-page": null;
    "pages/renew-password-page": null;
    "pages/grid-page": null;
    "pages/content-builder-page": null;
}

export const { route } = createRouter<RouteList>({
    index: "/",
    form: "/form-components",
    loader: "/loader",
    "forms/user-invitation": "/form/user-invitation",
    "pages/form-page": "/pages/form",
    "pages/forgotten-password-page": "/pages/forgotten-password",
    "pages/login-page": "/pages/login",
    "pages/renew-password-page": "/pages/renew-password",
    "pages/grid-page": "/pages/grid-page",
    "pages/content-builder-page": "/pages/content-builder-page",
});
