build:
	npm run build

dev:
	npm run dev

lint:
	npm run lint

test: lint typecheck

typecheck:
	npm run typecheck
