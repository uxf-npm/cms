import ExtendableError from "es6-error";
import { ValidationErrors } from "final-form";

export class ValidationError extends ExtendableError {
    private readonly _fields: ValidationErrors;

    constructor(fields: ValidationErrors) {
        super("Fields are not valid");
        this._fields = fields;
    }

    get fields(): ValidationErrors {
        return this._fields;
    }
}
