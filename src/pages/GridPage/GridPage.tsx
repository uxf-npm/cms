import Add from "@mui/icons-material/Add";
import Box from "@mui/material/Box";
import Fab from "@mui/material/Fab";
import Tooltip from "@mui/material/Tooltip";
import { DataGrid, DEFAULT_TOOLBAR_PLUGINS } from "@uxf/data-grid/tailwindui";
import { Loader, Request, Response, useCallbackRef } from "@uxf/data-grid";
import { queryParamToString } from "@uxf/router";
import { NextPage } from "next";
import Router from "next/router";
import qs from "qs";
import React, { useCallback, useMemo, useState } from "react";
import { parse as parseUrl } from "url";
import { userConfigGet } from "../../api";
import { withAuthenticate } from "../../deprecated/hoc/withAuthenticate";
import { createAxiosInstance } from "../../lib/api";
import { loadEntityGridSchema, loadEntityMetaSchema } from "./getInitialPropsHelper";
import { ActionsBag, GridPageComponentProps, GridPageConfig, LayoutComponent } from "./types";
import { useHiddenColumns } from "./useHiddenColumns";
import { container } from "../../config";

const { axiosRequest } = createAxiosInstance();

export const dataGridLoader = (gridName: string, request: Request): Promise<Response> => {
    return axiosRequest<Response>(null, `/api/cms/datagrid/${gridName}`, "get", null, request).then((r) => r.data);
};

const DefaultLayout: LayoutComponent = (props) => <div {...props} />;

const GridPageComponent: React.FC<GridPageComponentProps> = (props) => {
    const {
        ui = {},
        metaSchema,
        entityAlias,
        gridSchema,
        getOpenUrl,
        getEditUrl,
        onOpen,
        onAdd,
        onEdit,
        onRemove,
        query,
        initialHiddenColumns,
        loader,
        title,
        rowHeight,
        defaultConfig,
        AddIcon = <Add />,
        onAddTitle = "Přidat záznam",
        toolbarPlugins,
        selectable,
    } = props;
    const hiddenColumnProps = useHiddenColumns(entityAlias, initialHiddenColumns);
    const [selectedRows, setSelectedRows] = useState<Set<number>>(new Set());
    const { Layout = DefaultLayout, ...dataGridUi } = ui;

    const universalLoader = useCallback<Loader>(
        (gridName, request, encodedRequest) => {
            const response = dataGridLoader(gridName ?? "", request);

            // save filter to url
            const newQuery = { ...query, filter: encodedRequest };

            // TODO @vejvis - dodělat vlastní router a tuhle magii smazat!
            Router.replace(
                `${parseUrl(window.history.state?.url || "").pathname}?${qs.stringify(newQuery)}`,
                `${parseUrl(window.history.state?.as || "").pathname}?${qs.stringify(newQuery)}`,
                { shallow: true },
            );

            return response;
        },
        [query],
    );

    const callbackRef = useCallbackRef();

    const actionsBag = useMemo<ActionsBag>(
        () => ({
            reload: () => callbackRef.current?.reload(),
        }),
        [callbackRef],
    );

    return (
        <Layout key={entityAlias} title={title ?? metaSchema?.title ?? ""}>
            <Box mb={10}>
                <DataGrid
                    callbackRef={callbackRef}
                    schema={gridSchema}
                    initialState={query?.filter ? queryParamToString(query?.filter) : undefined}
                    title={title ?? metaSchema?.title}
                    onCsvDownload={(r) => window.open(`/api/cms/datagrid/export/${entityAlias}?${qs.stringify(r)}`)}
                    gridName={entityAlias}
                    loader={loader ?? universalLoader}
                    {...hiddenColumnProps}
                    noBorder
                    onOpen={onOpen && ((row) => onOpen(entityAlias, row, actionsBag))}
                    getOpenUrl={getOpenUrl && ((row) => getOpenUrl(entityAlias, row))}
                    onEdit={onEdit && ((row) => onEdit(entityAlias, row, actionsBag))}
                    getEditUrl={getEditUrl && ((row) => getEditUrl(entityAlias, row))}
                    onRemove={onRemove && ((row) => onRemove(entityAlias, row, actionsBag))}
                    rowHeight={rowHeight}
                    defaultConfig={defaultConfig}
                    ui={dataGridUi}
                    toolbarPlugins={toolbarPlugins ?? DEFAULT_TOOLBAR_PLUGINS}
                    selectedRows={selectable ? selectedRows : undefined}
                    onChangeSelectedRows={selectable ? setSelectedRows : undefined}
                />
            </Box>
            {onAdd && (!metaSchema || metaSchema?.actions.includes("add")) && (
                <Tooltip title={onAddTitle}>
                    <Fab
                        sx={{ position: "fixed", bottom: 16, right: 16 }}
                        onClick={() => onAdd(entityAlias, actionsBag)}
                        color="primary"
                    >
                        {AddIcon}
                    </Fab>
                </Tooltip>
            )}
        </Layout>
    );
};

GridPageComponent.displayName = "GridPageComponent";

export const GridPage = (config: GridPageConfig) => {
    const Component: NextPage<any> = (props) => {
        return (
            <GridPageComponent
                {...props}
                title={config.title}
                loader={config.loader}
                onEdit={config.onEdit}
                getEditUrl={config.getEditUrl}
                onAdd={config.onAdd}
                onRemove={config.onRemove}
                onOpen={config.onOpen}
                getOpenUrl={config.getOpenUrl}
                rowHeight={config.rowHeight}
                defaultConfig={config.defaultConfig}
                AddIcon={config.AddIcon}
                onAddTitle={config.onAddTitle}
                ui={config.ui}
                toolbarPlugins={config.toolbarPlugins}
                selectable={config.selectable}
            />
        );
    };

    Component.getInitialProps = async (ctx) => {
        const entityAlias = typeof config.entityAlias === "function" ? config.entityAlias(ctx) : config.entityAlias;
        const initialHiddenColumns = container.has("api.getUserConfig")
            ? await container.get("api.getUserConfig")(ctx, `v1--hidden-columns--${entityAlias}`, [])
            : await userConfigGet<string[]>(ctx, `v1--hidden-columns--${entityAlias}`, []);

        return {
            entityAlias,
            initialHiddenColumns,
            query: ctx.query,
            metaSchema: await loadEntityMetaSchema(ctx, entityAlias),
            gridSchema: await loadEntityGridSchema(ctx, entityAlias),
        };
    };

    return withAuthenticate({ allowedRoles: config.allowedRoles || ["ROLE_ROOT"] })(Component);
};
