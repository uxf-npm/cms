import { DataGridProps, Loader, Schema, ToolbarPlugin, UIComponents as DataGridUIComponents } from "@uxf/data-grid";
import { NextPageContext } from "next";
import { ParsedUrlQuery } from "querystring";
import { EntityMetaSchema } from "../../api";
import { LayoutProps } from "../../lib/Layout";
import { ReactElement } from "react";

export type LayoutComponent = React.FC<LayoutProps>;

export type ActionsBag = {
    reload: () => void;
};

export type GridPageComponentProps = {
    ui?: UIComponents;
    entityAlias: string;
    title?: string;
    initialHiddenColumns: string[];
    onOpen?: (entityAlias: string, row: any, actions: ActionsBag) => void;
    getOpenUrl?: (entityAlias: string, row: any) => string | null | undefined;
    onAdd?: (entityAlias: string, actions: ActionsBag) => void;
    onEdit?: (entityAlias: string, row: any, actions: ActionsBag) => void;
    getEditUrl?: (entityAlias: string, row: any) => string | null | undefined;
    onRemove?: (entityAlias: string, row: any, actions: ActionsBag) => void;
    query: ParsedUrlQuery;
    loader?: Loader;
    gridSchema: Schema<any>;
    metaSchema?: EntityMetaSchema;
    rowHeight?: DataGridProps<any, any>["rowHeight"];
    defaultConfig?: DataGridProps<any, any>["defaultConfig"];
    AddIcon?: ReactElement;
    onAddTitle?: string;
    toolbarPlugins?: ToolbarPlugin[];
    selectable?: boolean;
};

export type EntityAliasExtractor = (ctx: NextPageContext) => string;

type UIComponents = Partial<DataGridUIComponents<any, any>> & {
    Layout?: LayoutComponent;
};

export type GridPageConfig<T extends object = any> = {
    ui?: UIComponents;
    title?: string;
    entityAlias: string | EntityAliasExtractor;
    onOpen?: (entityAlias: string, row: T, actions: ActionsBag) => void;
    getOpenUrl?: (entityAlias: string, row: T) => string | undefined | null;
    onAdd?: (entityAlias: string, actions: ActionsBag) => void;
    onEdit?: (entityAlias: string, row: T, actions: ActionsBag) => void;
    getEditUrl?: (entityAlias: string, row: T) => string | undefined | null;
    onRemove?: (entityAlias: string, row: T, actions: ActionsBag) => void;
    allowedRoles?: string[];
    loader?: Loader;
    rowHeight?: DataGridProps<any, any>["rowHeight"];
    defaultConfig?: DataGridProps<any, any>["defaultConfig"];
    AddIcon?: ReactElement;
    onAddTitle?: string;
    toolbarPlugins?: ToolbarPlugin[];
    selectable?: boolean;
};
