import { ReactNode } from "react";

export interface ForgottenPasswordPageConfig {
    pageTitle?: string;
    title: ReactNode;
    onLogin?: () => void;
    onSubmit: (username: string) => Promise<void>;
    ui?: {
        Logo?: ReactNode;
    };
}
