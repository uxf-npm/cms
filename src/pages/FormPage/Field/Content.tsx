import React from "react";
import { ContentComponent, ContentField } from "../../ContentBuilder";
import { FieldProps } from "../types";
import { Wysiwyg } from "../../ContentBuilder/content/Wysiwyg";

export const Content: React.FC<FieldProps & { contentComponents?: ContentComponent<any>[] }> = (props) => {
    const { fieldSchema, prefix = "", contentComponents } = props;
    return (
        <fieldset>
            <legend>{fieldSchema.label}</legend>
            <ContentField contentComponents={contentComponents ?? [Wysiwyg]} name={`${prefix}${fieldSchema.name}`} />
        </fieldset>
    );
};
