import { EntityMetaSchema, FieldSchemaResponse, FormSchemaResponse } from "../../api";
import React, { ReactElement } from "react";

export interface FieldProps {
    fieldSchema: FieldSchemaResponse;
    entityAlias: string;
    isEditing?: boolean;
    prefix?: string;
}

export type FieldComponent = React.FC<FieldProps>;

export interface FormPageConfig<T = { id?: number | null }> {
    allowedRoles?: string[];
    initialValues?: ((ctx: any) => Promise<T>) | T;
    configureSchema?: (schema: FormSchemaResponse) => FormSchemaResponse;
    entityAlias?: string | ((ctx: any) => string);
    entityId?: number | ((ctx: any) => number);
    onSave?: (data: T, entityAlias: string) => Promise<T>;
    onSaveDone?: (data: T, entityAlias: string) => void;
    ui?: {
        Field?: FieldComponent;
        Layout?: ReactElement | ((entityMetaSchema: EntityMetaSchema) => ReactElement);
    };
}

export interface FormPageQueryParams {
    id?: string;
    entityAlias: string;
}
