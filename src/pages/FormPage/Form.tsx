import arrayMutators from "final-form-arrays";
import Grid from "@mui/material/Grid";
import { Fab } from "./styles";
import SaveIcon from "@mui/icons-material/Save";
import React from "react";
import { Form as ReactFinalForm, FormProps as ReactFinalFormProps } from "react-final-form";
import { FormSchemaResponse } from "../../api";
import { FieldComponent } from "./types";
import { BaseField } from "./BaseField";
import { FormState } from "final-form";

type SubmitButtonComponent = React.FC<{ formState: FormState<any> }>;

interface FormProps<T> extends ReactFinalFormProps<T> {
    className?: string;
    schema: FormSchemaResponse;
    Field?: FieldComponent;
    entityAlias: string;
    isEditing?: boolean;
    SubmitButton?: SubmitButtonComponent;
}

const BaseSubmitButton: SubmitButtonComponent = (props) => {
    const { formState } = props;
    return (
        <Fab title="Uložit" type="submit" disabled={formState.submitting || formState.pristine}>
            <SaveIcon />
        </Fab>
    );
};

export const Form = <T extends any>(props: FormProps<T>) => {
    const SubmitButton = props.SubmitButton ?? BaseSubmitButton;
    const Field = props.Field ?? BaseField;

    return (
        <ReactFinalForm initialValues={props.initialValues} onSubmit={props.onSubmit} mutators={{ ...arrayMutators }}>
            {(formState) => {
                return (
                    <form onSubmit={formState.handleSubmit} className={props.className ?? "pb-20"}>
                        <Grid container spacing={3}>
                            {props.schema.fields.map((field, i) => (
                                <Grid item key={i} xs={12}>
                                    <Field
                                        fieldSchema={field}
                                        entityAlias={props.entityAlias}
                                        isEditing={props.isEditing}
                                    />
                                </Grid>
                            ))}
                        </Grid>
                        <SubmitButton formState={formState} />
                    </form>
                );
            }}
        </ReactFinalForm>
    );
};
