import { FORM_ERROR } from "final-form";
import { NextPage } from "next";
import Router from "next/router";
import React, { useCallback, useEffect, useState } from "react";
import { FormProps } from "react-final-form";
import { Form } from "./Form";
import {
    EntityMetaSchema,
    FormSchemaResponse,
    getEntityMetaSchemas,
    getFormSchema,
    getFormValues,
    saveFormValues,
} from "../../api";
import { container } from "../../config";
import { withAuthenticate } from "../../deprecated/hoc/withAuthenticate";
import { ValidationError } from "../../errors/ValidationError";
import { Paper } from "../../ui/Paper";
import { BaseField } from "./BaseField";
import { FieldComponent, FormPageConfig } from "./types";
import { mapToRequestObject } from "./mapper";

export interface InitialProps<T extends { id?: number | null }> {
    formSchema: FormSchemaResponse;
    initialValues: T | null;
    entityAlias: string;
    entityId: number | null;
    entityMetaSchema: EntityMetaSchema;
}

const resolve = <T extends string | number>(
    value: T | undefined | null | ((value: T) => T | null),
    ctx: any,
): T | null => (typeof value === "function" ? value(ctx) : value ?? null);

export const FormPage = <T extends { id?: number | null }>(config: FormPageConfig<T>) => {
    const entityIdResolver = config.entityId ?? (({ query }) => (query.id ? Number.parseInt(query.id) : null));
    const entityAliasResolver =
        config.entityAlias ?? (({ query }) => (query.entityAlias ? `${query.entityAlias}` : null));

    const onSaveDone = config.onSaveDone ?? Router.back;

    const Component: NextPage<InitialProps<T>> = (props) => {
        const { formSchema, entityAlias, entityId, entityMetaSchema } = props;

        const [initialValues, setInitialValues] = useState<T | null>(props.initialValues);

        useEffect(() => {
            setInitialValues(props.initialValues);
        }, [props.initialValues]);

        const onSubmit = useCallback<FormProps<T>["onSubmit"]>(
            async (values) => {
                try {
                    const newInitialValues = config.onSave
                        ? await config.onSave(values, entityAlias)
                        : await saveFormValues<T>(
                              null,
                              { entityAlias, id: entityId },
                              mapToRequestObject(values, formSchema),
                          ).then((r) => r.data);

                    setInitialValues(newInitialValues);

                    onSaveDone(newInitialValues, entityAlias);

                    return;
                } catch (e) {
                    if (e instanceof ValidationError) {
                        console.error(e);
                        return e.fields;
                    }
                    container.get("service.error").handleError(e);
                    return { [FORM_ERROR]: "Error" }; // TODO
                }
            },
            [entityAlias, entityId, setInitialValues, formSchema],
        );

        const FieldComponent: FieldComponent = config.ui?.Field ?? BaseField;
        const Layout = config.ui?.Layout ?? <div />;

        const Content = (
            <Paper>
                <Form
                    initialValues={initialValues ?? {}}
                    schema={formSchema}
                    onSubmit={onSubmit}
                    Field={FieldComponent}
                    entityAlias={entityAlias}
                    isEditing={!!entityId}
                />
            </Paper>
        );

        return React.cloneElement(typeof Layout === "function" ? Layout(entityMetaSchema) : Layout, {
            children: Content,
        });
    };

    Component.getInitialProps = async (ctx) => {
        const entityId = resolve(entityIdResolver, ctx);
        const entityAlias = resolve(entityAliasResolver, ctx) ?? "";

        const metaSchemas = await getEntityMetaSchemas(ctx).then((r) => r.data);

        if (!metaSchemas[entityAlias]) {
            throw new Error(`Entity meta schema for alias ${entityAlias} does not exist.`);
        }

        const formSchema = await getFormSchema(ctx, { entityAlias }).then((r) => r.data);

        let initialValues = null;
        if (typeof config.initialValues === "function") {
            initialValues = await config.initialValues(ctx);
        } else if (config.initialValues) {
            initialValues = config.initialValues;
        } else if (entityId) {
            initialValues = await getFormValues(ctx, { entityAlias, id: entityId }).then((r) => r.data);
        }

        return {
            entityId,
            entityAlias,
            entityMetaSchema: metaSchemas[entityAlias],
            formSchema: config.configureSchema ? config.configureSchema(formSchema) : formSchema,
            initialValues,
        };
    };

    return withAuthenticate({ allowedRoles: config.allowedRoles || ["ROLE_ROOT"] })(Component);
};
