import { FormSchemaResponse } from "../../api";

export const mapToRequestObject = (values: any, schema: FormSchemaResponse): any => {
    const requestObject = { ...values };

    for (const field of schema.fields) {
        const { name, type } = field;
        const value = requestObject[name];
        switch (type) {
            case "file":
            case "image":
                requestObject[name] = value ? value.id : value;
                break;
            case "content":
                // TODO search string
                requestObject[name] = value ? { data: value.data ?? [], search: value.search ?? "" } : null;
                break;
            case "embedded":
                requestObject[name] = mapToRequestObject(value, field);
                break;
        }
    }

    return requestObject;
};
