import { queryParamToStringStrict } from "@uxf/router";
import { NextPage } from "next";
import React, { useCallback } from "react";
import { RenewPasswordForm, RenewPasswordFormData, RenewPasswordFormProps } from "../../forms/RenewPasswordForm";
import { LoginLayout } from "../../lib/LoginLayout";
import { RenewPasswordPageConfig } from "./types";

const INITIAL_VALUES: RenewPasswordFormData = {
    password: "",
    passwordAgain: "",
};

interface PageProps {
    token: string;
}

export const RenewPasswordPage = (config: RenewPasswordPageConfig) => {
    const Component: NextPage<PageProps> = (props) => {
        const onSubmit = useCallback<RenewPasswordFormProps["onSubmit"]>(
            async (values) => {
                await config.onSubmit(props.token, values);
            },
            [props.token],
        );

        return (
            <LoginLayout title={config.title} pageTitle={config.pageTitle} Logo={config.ui?.Logo}>
                <RenewPasswordForm onSubmit={onSubmit} initialValues={INITIAL_VALUES} />
            </LoginLayout>
        );
    };

    Component.getInitialProps = async (ctx) => {
        const token = queryParamToStringStrict(ctx.query.token);

        if (config.checkToken) {
            await config.checkToken(ctx, token);
        }

        return { token };
    };

    return Component;
};
