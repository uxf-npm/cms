import { ContentProps } from "./types";
import React, { useCallback } from "react";
import Typography from "@mui/material/Typography";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import Button from "@mui/material/Button";

export const ContentHeader: React.FC<ContentProps> = (props) => {
    const { index, name, TypeInputProps, onRemove } = props;

    const inputId = `field-${name}`;

    const removeHandler = useCallback(() => {
        // eslint-disable-next-line no-alert
        if (confirm("Opravdu chcete smazat sekci?")) {
            onRemove();
        }
    }, [onRemove]);

    return (
        <div>
            <div className="flex flex-row justify-between items-center pt-8 pb-4">
                <Typography variant="h3" fontWeight="bold">
                    Sekce #{index + 1}
                </Typography>
                {index > 0 ? (
                    <div>
                        <Button size="small" color="error" onClick={removeHandler}>
                            Smazat sekci
                        </Button>
                    </div>
                ) : (
                    <div />
                )}
            </div>
            <div className="pt-2">
                <FormControl fullWidth>
                    <InputLabel id={inputId}>Typ</InputLabel>
                    <Select label="Typ" size="small" labelId={inputId} {...TypeInputProps}>
                        {TypeInputProps.options.map(({ children, ...menuItemProps }) => {
                            return (
                                <MenuItem key={index} {...menuItemProps}>
                                    {children}
                                </MenuItem>
                            );
                        })}
                    </Select>
                </FormControl>
            </div>
        </div>
    );
};
