import { MenuItemProps } from "@mui/material/MenuItem";
import { NextPageContext } from "next";
import React, { ReactNode } from "react";
import { FieldInputProps } from "react-final-form";
import { ContentResponse, ContentTagResponse } from "../../api";
import { FileResponse } from "../../ui/FileUploadInput";

export type VisibilityLevel = "PUBLIC" | "PUBLIC_WITHOUT_SITEMAP" | "PRIVATE"; // TODO duplicated in api

export interface ContentProps {
    name: string;
    index: number;
    onRemove: () => void;
    TypeInputProps: FieldInputProps<string> & { options: MenuItemProps[] };
}

export type ContentGetConfig<T extends Content<string, any>> = () => {
    type: T["type"];
    label: ReactNode;
};

export type ContentComponent<
    T extends Content<string, any>,
    Props extends ContentProps = ContentProps,
> = React.FC<Props> & {
    getConfig: ContentGetConfig<T>;
};

export type ContentPageType = "BLOG" | "LANDING_PAGE" | "QUESTION" | "TERM";

export interface ContentBuilderConfig<Contents extends Content<string, any>> {
    type?: ContentPageType;
    allowedTypes?: Array<{ id: ContentPageType; label: string }>;
    // TODO rozmapovat contents do content component
    // TODO https://stackoverflow.com/questions/51691235/typescript-map-union-type-to-another-union-type
    contentComponents: ContentComponent<any>[];
    onSubmit?: (contentId: number | null, values: RootContent<Contents>) => Promise<ContentResponse>;
    onSubmitSuccess?: (
        contentId: number | null,
        values: RootContent<Contents>,
        response: ContentResponse,
    ) => Promise<void>;
    initialValues?: RootContent<Contents> | ((ctx: NextPageContext) => Promise<RootContent<Contents>>);
    allowedRoles?: string[];
    ui: {
        Layout?: React.FC<{ children?: ReactNode }>;
    };
    hide?: {
        author?: boolean;
        category?: boolean;
        image?: boolean;
        perex?: boolean;
        publishedAt?: boolean;
        seo?: {
            title?: boolean;
            description?: boolean;
            ogTitle?: boolean;
            ogDescription?: boolean;
            ogImage?: boolean;
        };
    };
}

export interface Content<Type extends string, Content> {
    type: Type;
    content: Content;
}

export type RootContent<T> = {
    type: string | null;
    active: boolean;
    visibilityLevel: VisibilityLevel;
    name: string;
    publishedAt: string;
    author: { id: number; label: string } | null;
    category: { id: number; label: string } | null;
    image: FileResponse | null;
    perex: string;
    seo: {
        title: string;
        description: string;
        ogTitle: string;
        ogDescription: string;
        name: string | null;
        ogImage: FileResponse | null;
    };
    content: {
        data: T[];
        search?: string;
    };
    tags?: ContentTagResponse[];
};
