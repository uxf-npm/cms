import { RootContent } from "./types";
import { ContentRequestBody, ContentResponse } from "../../api";

export function mapResponseToFormData(response: ContentResponse): RootContent<any> {
    return {
        type: response.type || null,
        name: response.name,
        perex: response.perex,
        seo: {
            name: response.seo.name,
            title: response.seo.title,
            description: response.seo.description,
            ogTitle: response.seo.ogTitle,
            ogDescription: response.seo.ogDescription,
            ogImage: response.seo.ogImage,
        },
        visibilityLevel: response.visibilityLevel,
        image: response.image,
        content: response.content as any,
        author: response.author
            ? {
                  id: response.author.id,
                  label: `${response.author.firstName} ${response.author.surname}`,
              }
            : null,
        category: response.category
            ? {
                  id: response.category.id,
                  label: `${response.category.name}`,
              }
            : null,
        publishedAt: response.publishedAt ?? new Date().toISOString(),
        active: !response.hidden,
        tags: response.tags ?? [],
    };
}

export function mapFormDataToRequest(values: RootContent<any>): ContentRequestBody {
    return {
        name: values.name,
        perex: values.perex,
        type: values.type ?? "",
        seo: {
            name: values.seo.name ?? "",
            title: values.seo.title,
            description: values.seo.description,
            ogTitle: values.seo.ogTitle,
            ogDescription: values.seo.ogDescription,
            ogImage: values.seo.ogImage?.id ?? null,
        },
        hidden: !values.active,
        category: values.category?.id ?? null,
        author: values.author?.id ?? null,
        publishedAt: values.publishedAt,
        visibilityLevel: values.visibilityLevel,
        image: values.image?.id ?? null,
        tags: values.tags?.map((t) => t.id) ?? [],
        content: {
            data: values.content as any,
            search: "", // TODO
        },
    };
}
