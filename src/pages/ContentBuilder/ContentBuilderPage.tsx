import { NextPage } from "next";
import React, { useCallback } from "react";
import { Content, ContentBuilderConfig, RootContent } from "./types";
import { Form, FormProps } from "./components/Form";
import { withAuthenticate } from "../../deprecated/hoc/withAuthenticate";
import Box from "@mui/material/Box";
import { contentCreate, contentGet, contentUpdate } from "../../api";
import { queryParamToNumber } from "@uxf/router";
import { mapFormDataToRequest, mapResponseToFormData } from "./mapper";
import { container } from "../../config";

const DefaultLayout: React.FC = (props) => <div {...props} />;

const defaultInitialValues: ContentBuilderConfig<any>["initialValues"] = async (ctx) => {
    const contentId = ctx.query.id ? queryParamToNumber(ctx.query.id) : null;

    const date = new Date();
    date.setHours(0);
    date.setMinutes(0);
    date.setMilliseconds(0);

    return contentId
        ? await contentGet(ctx, contentId).then((r) => mapResponseToFormData(r.data))
        : {
              name: "",
              type: null,
              author: null,
              category: null,
              content: { data: [] },
              perex: "",
              visibilityLevel: "PRIVATE",
              seo: {
                  name: null,
                  title: "",
                  ogTitle: "",
                  ogDescription: "",
                  description: "",
                  ogImage: null,
              },
              image: null,
              publishedAt: date.toISOString(),
              active: true,
              tags: [],
          };
};

const defaultOnSubmit: ContentBuilderConfig<any>["onSubmit"] = async (id, values) => {
    return id
        ? contentUpdate(null, id, mapFormDataToRequest(values)).then((r) => r.data)
        : contentCreate(null, mapFormDataToRequest(values)).then((r) => r.data);
};

const defaultOnSubmitSuccess: ContentBuilderConfig<any>["onSubmitSuccess"] = async (id) => {
    container.get("service.notification").success(id ? "Změny uloženy" : "Článek vytvořen");
};

const ContentBuilderPageComponent: React.FC<{
    config: ContentBuilderConfig<any>;
    initialValues: RootContent<any>;
    id: number | null;
}> = (props) => {
    const { config, initialValues, id } = props;
    const { ui } = config;

    const Layout = ui.Layout ?? DefaultLayout;
    const onSubmit = config.onSubmit ?? defaultOnSubmit;
    const onSubmitSuccess = config.onSubmitSuccess ?? defaultOnSubmitSuccess;

    const handleSubmit = useCallback<FormProps["onSubmit"]>(
        async (values) => {
            const preparedValues = config.type ? { ...values, type: config.type } : values;
            const response = await onSubmit(id, preparedValues);
            await onSubmitSuccess(id, preparedValues, response);
        },
        [onSubmit, onSubmitSuccess, id, config],
    );

    return (
        <Layout>
            <Box pb={8}>
                <Form config={config} id={id} onSubmit={handleSubmit} initialValues={initialValues} />
            </Box>
        </Layout>
    );
};
ContentBuilderPageComponent.displayName = "ContentBuilderComponent";

export const ContentBuilderPage = <T extends Content<string, any>>(config: ContentBuilderConfig<T>) => {
    const Page: NextPage<any, { id: number | null; initialValues: RootContent<any> }> = (props) => {
        const { initialValues, id } = props;

        return <ContentBuilderPageComponent config={config} initialValues={initialValues} id={id} />;
    };

    Page.getInitialProps = async (ctx) => {
        const contentId = queryParamToNumber(ctx.query.id) ?? null; // TODO @vejvis - nekompatibilní s initial values
        const initialValues = config.initialValues ?? defaultInitialValues;

        return {
            id: contentId,
            initialValues: typeof initialValues === "function" ? await initialValues(ctx) : initialValues,
        };
    };

    return withAuthenticate({ allowedRoles: config.allowedRoles || ["ROLE_ROOT"] })(Page);
};
