import { WysiwygContent } from "@uxf/wysiwyg";
import React from "react";
import { TextInput } from "../../../forms/components/TextInput";
import { WysiwygField } from "../../../forms/components/WysiwygField";
import { ContentHeader } from "../ContentHeader";
import { Content, ContentComponent, ContentProps } from "../types";

export type HeroSectionFormData = {
    title: string;
    subtitle?: string;
    text?: WysiwygContent;
    ctaUrl?: string;
};

type Props = ContentProps;

export type HeroSectionContent = Content<"heroSection", HeroSectionFormData>;

export const HeroSection: ContentComponent<HeroSectionContent, Props> = (props) => {
    const { name } = props;
    return (
        <div>
            <ContentHeader {...props} />
            <div className="pt-6">
                <div className="grid grid-cols-12 gap-4 mt-2">
                    <div className="col-span-12">
                        <TextInput name={`${name}.title`} label="Nadpis" required />
                    </div>
                    <div className="col-span-12">
                        <TextInput name={`${name}.subtitle`} label="Podnadpis" />
                    </div>
                    <div className="col-span-12">
                        <WysiwygField name={`${name}.text`} />
                    </div>
                    <div className="col-span-12">
                        <TextInput name={`${name}.ctaUrl`} label="CTA url" />
                    </div>
                </div>
            </div>
        </div>
    );
};

HeroSection.getConfig = () => ({
    type: "heroSection",
    label: "Hero sekce",
});
