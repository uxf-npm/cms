import React from "react";
import { Content, ContentComponent, ContentProps } from "../types";
import { ContentHeader } from "../ContentHeader";
import { FileResponse } from "../../../ui/FileUploadInput";
import { DropZone } from "../../../forms/components/DropZone";

export type GalleryFormData = {
    images?: FileResponse[];
};

export type GalleryContent = Content<"gallery", GalleryFormData>;

type Props = ContentProps;

export const Gallery: ContentComponent<GalleryContent, Props> = (props) => {
    const { name } = props;

    return (
        <div>
            <ContentHeader {...props} />
            <div className="pt-6">
                <DropZone name={`${name}.images`} />
            </div>
        </div>
    );
};

Gallery.getConfig = () => ({
    type: "gallery",
    label: "Galerie",
});
