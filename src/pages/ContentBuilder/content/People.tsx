import React from "react";
import { Content, ContentComponent, ContentProps } from "../types";
import Box from "@mui/material/Box";
import { ContentHeader } from "../ContentHeader";
import { TextInput } from "../../../forms/components/TextInput";
import { FileUploadInput } from "../../../forms/components/FileUploadInput";
import { FieldArray } from "react-final-form-arrays";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { FileResponse } from "../../../ui/FileUploadInput";

export type PersonFormData = {
    name?: string;
    subtitle?: string;
    image?: FileResponse;
    text?: string;
    phone?: string;
    email?: string;
};

export type PeopleFormData = {
    people: PersonFormData[];
};

export type PeopleContent = Content<"people", PeopleFormData>;

type Props = ContentProps;

export const People: ContentComponent<PeopleContent, Props> = (props) => {
    const { name } = props;

    return (
        <div>
            <ContentHeader {...props} />
            <div className="pt-6">
                <FieldArray name={`${name}.people`}>
                    {({ fields }) => {
                        return (
                            <>
                                {fields.map((fieldName, index) => {
                                    const handleRemove = () => {
                                        // eslint-disable-next-line no-alert
                                        if (confirm("Opravdu chcete smazat osobu?")) {
                                            fields.remove(index);
                                        }
                                    };
                                    return (
                                        <div key={index} className="border-l-2 border-primary pl-8 mb-8">
                                            <div className="flex justify-between">
                                                <Typography variant="h4" pt={1} mb={3} fontWeight="bold">
                                                    Osoba #{index + 1}
                                                </Typography>
                                                {index > 0 ? (
                                                    <div>
                                                        <Button size="small" color="error" onClick={handleRemove}>
                                                            Smazat osobu
                                                        </Button>
                                                    </div>
                                                ) : (
                                                    <Box />
                                                )}
                                            </div>
                                            <div className="grid grid-cols-12 gap-4 mt-2">
                                                <div className="col-span-12">
                                                    <TextInput name={`${fieldName}.name`} label="Jméno" required />
                                                </div>
                                                <div className="col-span-12">
                                                    <FileUploadInput
                                                        id={`${fieldName}.image`}
                                                        name={`${fieldName}.image`}
                                                        label="Obrázek"
                                                    />
                                                </div>
                                                <div className="col-span-12">
                                                    <TextInput name={`${fieldName}.subtitle`} label="Popisek" />
                                                </div>
                                                <div className="col-span-12">
                                                    <TextInput
                                                        name={`${fieldName}.text`}
                                                        label="Text"
                                                        multiline
                                                        rows="2"
                                                    />
                                                </div>
                                                <div className="col-span-6">
                                                    <TextInput name={`${fieldName}.phone`} label="Telefon" type="tel" />
                                                </div>
                                                <div className="col-span-6">
                                                    <TextInput
                                                        name={`${fieldName}.email`}
                                                        label="E-mail"
                                                        type="email"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })}
                                <Button variant="outlined" onClick={() => fields.push({ name: "" })}>
                                    Přidat osobu
                                </Button>
                            </>
                        );
                    }}
                </FieldArray>
            </div>
        </div>
    );
};

People.getConfig = () => ({
    type: "people",
    label: "Lidé",
});
