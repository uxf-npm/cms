import { Box } from "@mui/material";
import Typography from "@mui/material/Typography";
import { WysiwygContent } from "@uxf/wysiwyg";
import React from "react";
import { FieldArray } from "react-final-form-arrays";
import { FileUploadInput } from "../../../forms/components/FileUploadInput";
import { TextInput } from "../../../forms/components/TextInput";
import { WysiwygField } from "../../../forms/components/WysiwygField";
import { Button } from "../../../ui/Button";
import { FileResponse } from "../../../ui/FileUploadInput";
import { ContentHeader } from "../ContentHeader";
import { Content, ContentComponent, ContentProps } from "../types";

export type FeatureSectionFormData = {
    title: string;
    subtitle?: string;
    features: Array<{
        title: string;
        image?: FileResponse;
        text?: WysiwygContent;
        ctaUrl?: string;
    }>;
};

type Props = ContentProps;

export type FeatureSectionContent = Content<"featureSection", FeatureSectionFormData>;

export const FeatureSection: ContentComponent<FeatureSectionContent, Props> = (props) => {
    const { name } = props;
    return (
        <div>
            <ContentHeader {...props} />
            <div className="pt-6">
                <div className="grid grid-cols-12 gap-4 mt-2">
                    <div className="col-span-12">
                        <TextInput name={`${name}.title`} label="Nadpis" required />
                    </div>
                    <div className="col-span-12">
                        <TextInput name={`${name}.subtitle`} label="Podnadpis" />
                    </div>
                    <div className="col-span-12">
                        <FieldArray name={`${name}.features`}>
                            {({ fields }) => {
                                return (
                                    <>
                                        {fields.map((fieldName, index) => {
                                            const handleRemove = () => {
                                                // eslint-disable-next-line no-alert
                                                if (confirm("Opravdu chcete smazat feature?")) {
                                                    fields.remove(index);
                                                }
                                            };
                                            return (
                                                <div
                                                    key={`feature-${index}`}
                                                    className="border-l-2 border-primary pl-8 mb-8"
                                                >
                                                    <div className="flex justify-between">
                                                        <Typography variant="h4" pt={1} mb={3} fontWeight="bold">
                                                            Feature #{index + 1}
                                                        </Typography>
                                                        {index > 0 ? (
                                                            <div>
                                                                <Button
                                                                    size="small"
                                                                    color="error"
                                                                    onClick={handleRemove}
                                                                >
                                                                    Smazat feature
                                                                </Button>
                                                            </div>
                                                        ) : (
                                                            <Box />
                                                        )}
                                                    </div>
                                                    <div className="grid grid-cols-12 gap-4 mt-2">
                                                        <div className="col-span-12">
                                                            <TextInput
                                                                name={`${fieldName}.title`}
                                                                label="Nadpis"
                                                                required
                                                            />
                                                        </div>
                                                        <div className="col-span-12">
                                                            <FileUploadInput
                                                                id={`${fieldName}.image`}
                                                                name={`${fieldName}.image`}
                                                                label="Obrázek"
                                                            />
                                                        </div>
                                                        <div className="col-span-12">
                                                            <WysiwygField name={`${fieldName}.text`} />
                                                        </div>
                                                        <div className="col-span-12">
                                                            <TextInput name={`${fieldName}.ctaUrl`} label="CTA url" />
                                                        </div>
                                                    </div>
                                                </div>
                                            );
                                        })}
                                        <Button variant="outlined" onClick={() => fields.push({ title: "" })}>
                                            Přidat feature
                                        </Button>
                                    </>
                                );
                            }}
                        </FieldArray>
                    </div>
                </div>
            </div>
        </div>
    );
};

FeatureSection.getConfig = () => ({
    type: "featureSection",
    label: "Feature sekce",
});
