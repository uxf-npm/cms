import React from "react";
import { Content, ContentComponent, ContentGetConfig, ContentProps } from "../types";
import Box from "@mui/material/Box";
import { EditorImageElementPlugin, EditorPlugin, WysiwygEditor, WysiwygEditorProps } from "@uxf/wysiwyg/Editor";
import { useField } from "react-final-form";
import { ContentHeader } from "../ContentHeader";
import {
    Toolbar,
    ParagraphPlugin,
    BoldPlugin,
    LinkPlugin,
    ItalicPlugin,
    ListItemPlugin,
    UnderlinePlugin,
    HeadingPlugins,
    Divider as __Divider__,
    ListBulletedPlugin,
    ListNumberedPlugin,
    CodePlugin,
    BlockQuotePlugin,
    ImagePlugin,
} from "@uxf/wysiwyg-mui5-plugins";
import { container } from "../../../config";

export type WysiwygContent = Content<"wysiwyg", any>;

type ConfigProps = {
    editorProps?: Partial<Omit<WysiwygEditorProps, "value" | "onChange">>;
};

type Props = ContentProps & ConfigProps;

export const DefaultImagePlugin: EditorImageElementPlugin = {
    ...ImagePlugin,
    imageNamespace: "default",
    imageUrlClient: (uuid, extension, namespace) =>
        `/upload/${namespace}/${uuid.charAt(0)}/${uuid.charAt(1)}/${uuid}.${extension}`,
    imageUploadHandler: container.get("api.uploadFile") as any,
};

export const DEFAULT_EDITOR_PLUGINS: EditorPlugin[] = [
    ParagraphPlugin,
    ...HeadingPlugins,
    __Divider__,
    BoldPlugin,
    ItalicPlugin,
    UnderlinePlugin,
    __Divider__,
    LinkPlugin,
    BlockQuotePlugin,
    CodePlugin,
    __Divider__,
    ListBulletedPlugin,
    ListNumberedPlugin,
    ListItemPlugin,
    __Divider__,
    DefaultImagePlugin,
];

export const Wysiwyg: ContentComponent<WysiwygContent, Props> & {
    create: (config: ConfigProps) => ContentComponent<WysiwygContent, Props>;
} = (props) => {
    const { name, editorProps } = props;
    const { input } = useField(name, { allowNull: true });

    const value = Array.isArray(input.value) ? input.value : input.value?.content ?? null; // backward compatibility

    return (
        <Box>
            <Box pb={2}>
                <ContentHeader {...props} />
            </Box>
            <WysiwygEditor
                value={Array.isArray(value) && value.length === 0 ? null : value} // empty array is not valid
                onChange={input.onChange}
                Toolbar={editorProps?.Toolbar ?? Toolbar}
                plugins={editorProps?.plugins ?? DEFAULT_EDITOR_PLUGINS}
                stylesForEditable={
                    editorProps?.stylesForEditable ?? {
                        border: "1px solid #e1e1e1",
                        padding: 12,
                        borderBottomLeftRadius: 4,
                        borderBottomRightRadius: 4,
                        borderTop: 0,
                    }
                }
            />
        </Box>
    );
};

const defaultGetConfig: ContentGetConfig<WysiwygContent> = () => ({
    type: "wysiwyg",
    label: "Strukturovaný text",
});

Wysiwyg.create = (config = {}) => {
    const { editorProps } = config;
    const Component: ContentComponent<WysiwygContent> = (props) => <Wysiwyg {...props} editorProps={editorProps} />;
    Component.getConfig = defaultGetConfig;

    return Component;
};

Wysiwyg.getConfig = defaultGetConfig;
