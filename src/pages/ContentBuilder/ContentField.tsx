import Grid from "@mui/material/Grid";
import { FormContent } from "./components/FormContent";
import Button from "@mui/material/Button";
import { FieldArray } from "react-final-form-arrays";
import React from "react";
import { ContentComponent } from "./types";
import Box from "@mui/material/Box";
import AddIcon from "@mui/icons-material/Add";
import { useField } from "react-final-form";

interface ContentFieldProps {
    contentComponents: ContentComponent<any>[];
    name: string;
}

export const ContentField: React.FC<ContentFieldProps> = (props) => {
    const { contentComponents, name } = props;

    useField(`${name}.search`, { defaultValue: "" });

    return (
        <FieldArray name={`${name}.data`}>
            {({ fields }) => (
                <Grid container spacing={2}>
                    {fields.map((name, index) => (
                        <Grid key={name} item xs={12}>
                            <FormContent
                                name={name}
                                contentComponents={contentComponents}
                                index={index}
                                onRemove={() => fields.remove(index)}
                            />
                        </Grid>
                    ))}
                    <Grid item xs={12}>
                        <Box py={1}>
                            <Button
                                startIcon={<AddIcon />}
                                variant="outlined"
                                type="button"
                                onClick={() =>
                                    fields.push({
                                        type: contentComponents[0]?.getConfig().type ?? "unknown",
                                        content: null,
                                    })
                                }
                            >
                                Přidat sekci
                            </Button>
                        </Box>
                    </Grid>
                </Grid>
            )}
        </FieldArray>
    );
};
