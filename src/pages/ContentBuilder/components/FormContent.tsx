import React from "react";
import { useField } from "react-final-form";
import WarningIcon from "@mui/icons-material/Warning";
import Typography from "@mui/material/Typography";
import { ContentComponent, ContentProps } from "../types";

interface FormSectionProps extends Omit<ContentProps, "TypeInputProps"> {
    contentComponents: ContentComponent<any>[];
}

const getSection = (sections: ContentComponent<any>[], type: string): ContentComponent<any> | undefined => {
    return sections.find((s) => s.getConfig().type === type);
};

export const FormContent: React.FC<FormSectionProps> = (props) => {
    const { contentComponents, onRemove, name, index } = props;

    const { input } = useField(`${name}.type`);

    const SectionContent = getSection(contentComponents, input.value);

    if (SectionContent) {
        return (
            <SectionContent
                name={`${name}.content`}
                onRemove={onRemove}
                index={index}
                TypeInputProps={{
                    ...input,
                    options: contentComponents.map((s) => ({
                        value: s.getConfig().type,
                        children: s.getConfig().label,
                    })),
                }}
            />
        );
    }

    return (
        <div className="flex align-center justify-center p-4">
            <WarningIcon color="error" />
            <Typography color="error" ml={2} fontWeight="bold">
                Nepodporovaná sekce
            </Typography>
        </div>
    );
};
