import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import { Checkbox } from "../../../forms/components/Checkbox";
import { TextInput } from "../../../forms/components/TextInput";
import React, { ReactNode } from "react";
import { DateTimePicker } from "../../../forms/components/DateTimePicker";
import { Select } from "../../../forms/components/Select";
import { ContentBuilderConfig, VisibilityLevel } from "../types";
import { FileUploadInput } from "../../../forms/components/FileUploadInput";
import { Autocomplete } from "../../../forms/components/Autocomplete";
import { autocomplete as autocompleteLoader } from "../../../api";
import { AutocompleteMultiple } from "../../../forms/components/AutocompleteMultiple";

const options: Array<{ id: VisibilityLevel; label: string }> = [
    { id: "PRIVATE", label: "Privátní" },
    { id: "PUBLIC_WITHOUT_SITEMAP", label: "Veřejný bez sitemapy" },
    { id: "PUBLIC", label: "Veřejný" },
];

interface Props {
    config: ContentBuilderConfig<any>;
    id: number | null;
    children?: ReactNode;
}

export const FormRootFields: React.FC<Props> = (props) => {
    const { children, config, id } = props;

    return (
        <div className="pb-2 pt-4">
            <Grid container spacing={2} px={2} pb={2}>
                {config.allowedTypes && (
                    <Grid item xs={12}>
                        <Select name="type" label="Typ" options={config.allowedTypes} readOnly={!!id} required />
                    </Grid>
                )}
                <Grid item xs={12}>
                    <TextInput name="name" label="Nadpis" required />
                </Grid>
                {!config.hide?.publishedAt && (
                    <Grid item xs={12}>
                        <DateTimePicker name="publishedAt" label="Datum publikace" required />
                    </Grid>
                )}
                {!config.hide?.perex && (
                    <Grid item xs={12}>
                        <TextInput name="perex" label="Perex" multiline minRows={3} />
                    </Grid>
                )}
                {!config.hide?.author && (
                    <Grid item xs={12}>
                        <Autocomplete
                            name="author"
                            label="Autor"
                            loadOptions={(term) =>
                                autocompleteLoader(null, { name: "content-author" }, { term }).then((r) => r.data)
                            }
                        />
                    </Grid>
                )}
                {!config.hide?.category && (
                    <Grid item xs={12}>
                        <Autocomplete
                            name="category"
                            label="Kategorie"
                            loadOptions={(term) =>
                                autocompleteLoader(null, { name: "content-category" }, { term }).then((r) => r.data)
                            }
                        />
                    </Grid>
                )}
                <Grid item xs={12}>
                    <AutocompleteMultiple
                        name="tags"
                        label="Tagy"
                        loadOptions={(term) =>
                            autocompleteLoader(null, { name: "content-tag" }, { term }).then((r) => r.data)
                        }
                    />
                </Grid>
                {!config.hide?.image && (
                    <Grid item xs={12}>
                        <FileUploadInput id="image" name="image" label="Hlavní obrázek" />
                    </Grid>
                )}
                <Grid item xs={12} md={6}>
                    <Checkbox name="active" label="Aktivní" />
                </Grid>
                <Grid item xs={12} md={6}>
                    <Select name="visibilityLevel" label="Viditelnost" options={options} required />
                </Grid>
            </Grid>
            <Typography variant="h2" pt={6} pb={4} px={2} style={{ fontWeight: 500 }}>
                SEO
            </Typography>
            <Grid container spacing={2} p={2}>
                <Grid item xs={12}>
                    <TextInput name="seo.name" label="Slug" helperText="Slug = unikátní koncovka URL adresy" />
                </Grid>
                {!config.hide?.seo?.title && (
                    <Grid item xs={12}>
                        <TextInput name="seo.title" label="Nadpis" />
                    </Grid>
                )}
                {!config.hide?.seo?.description && (
                    <Grid item xs={12}>
                        <TextInput name="seo.description" label="Popis" />
                    </Grid>
                )}
                {!config.hide?.seo?.ogTitle && (
                    <Grid item xs={12}>
                        <TextInput
                            name="seo.ogTitle"
                            label="OG nadpis"
                            helperText="Nadpis který bude zobrazen při sdílení na Facebooku, ..."
                        />
                    </Grid>
                )}
                {!config.hide?.seo?.ogDescription && (
                    <Grid item xs={12}>
                        <TextInput
                            name="seo.ogDescription"
                            label="OG popis"
                            helperText="Popis který bude zobrazen při sdílení na Facebooku, ..."
                        />
                    </Grid>
                )}
                {!config.hide?.seo?.ogImage && (
                    <Grid item xs={12}>
                        <FileUploadInput
                            id="seo.ogImage"
                            name="seo.ogImage"
                            label="OG obrázek"
                            helperText="Obrázek který bude zobrazen při sdílení na Facebooku, ..."
                        />
                    </Grid>
                )}
            </Grid>
            {children}
        </div>
    );
};
