import React from "react";
import { Form as ReactFinalForm, FormProps as ReactFinalFormProps } from "react-final-form";
import arrayMutators from "final-form-arrays";
import SaveIcon from "@mui/icons-material/Save";
import Fab from "@mui/material/Fab";
import { ContentBuilderConfig, RootContent } from "../types";
import { FormRootFields } from "./FormRootFields";
import { ContentField } from "../ContentField";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import createDecorator from "final-form-calculate";
import { slugify } from "@uxf/core/utils/slugify";

export interface FormProps extends ReactFinalFormProps<RootContent<any>> {
    config: ContentBuilderConfig<any>;
    id: number | null;
}
// Create Decorator
const decorator = createDecorator(
    // Calculations:
    {
        field: "name", // when minimum changes...
        updates: {
            // ...update maximum to the result of this function
            "seo.name": (nameValue, allValues: any, prevValues: any) => {
                return !allValues.seo.name || allValues.seo.name === slugify(prevValues.name ?? "")
                    ? slugify(nameValue ?? "")
                    : allValues.seo.name;
            },
        },
    },
);
export const Form: React.FC<FormProps> = (props) => {
    const { config, id, ...formProps } = props;

    return (
        <ReactFinalForm
            {...formProps}
            mutators={{
                ...arrayMutators,
            }}
            decorators={[decorator as any]}
        >
            {({ handleSubmit, submitting, pristine }) => (
                <form onSubmit={handleSubmit}>
                    <FormRootFields config={config} id={id}>
                        <Box px={2}>
                            <Typography variant="h2" pt={6} pb={2} style={{ fontWeight: 500 }}>
                                Obsah
                            </Typography>
                            <ContentField name="content" contentComponents={config.contentComponents} />
                        </Box>
                    </FormRootFields>
                    <Fab
                        color="primary"
                        type="submit"
                        sx={{ position: "fixed", bottom: 16, right: 16 }}
                        disabled={submitting || pristine}
                    >
                        <SaveIcon />
                    </Fab>
                </form>
            )}
        </ReactFinalForm>
    );
};
