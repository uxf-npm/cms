import { UnauthorizedError } from "../../errors/UnauthorizedError";
import { getLoggedUser } from "../../api";
import { onUserPersist } from "../redux/user";
import { NextComponentType } from "next";
import React from "react";
import { IAppContext } from "../types/App";
import CmsAuthorizator from "../security/CmsAuthorizator";
import { container } from "../../config";

export interface IConfig {
    allowedRoles?: string[];
}

// TODO component any
export const withAuthenticate =
    <PROPS extends object, INITIAL_PROPS extends object>(config: IConfig) =>
    (Component: any): NextComponentType<IAppContext, INITIAL_PROPS, PROPS> =>
        class extends React.Component<PROPS> {
            static displayName = "withAuthenticateHOC";
            public static async getInitialProps(ctx: IAppContext) {
                let user = ctx.reduxStore.getState().user.user;
                if (!user) {
                    user = container.has("api.getLoggedUser")
                        ? await container.get("api.getLoggedUser")(ctx)
                        : await getLoggedUser(ctx);
                    ctx.reduxStore.dispatch(onUserPersist(user));
                }

                if (config.allowedRoles && !CmsAuthorizator.isGranted(config.allowedRoles, user?.roles ?? [])) {
                    throw new UnauthorizedError();
                }

                return typeof Component.getInitialProps === "function" ? await Component.getInitialProps(ctx) : {};
            }

            public render(): JSX.Element {
                return <Component {...this.props} />;
            }
        } as any; // TODO!
