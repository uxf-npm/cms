import { ActionTypes, IState } from "./actions";

const initialState: IState = {
    meta: {},
    form: {},
    grid: {},
};

const entitiesReducer = (state: IState = initialState, action: ActionTypes): IState => {
    // tslint:disable-next-line:no-small-switch
    switch (action.type) {
        case "entities/SET_META_SCHEMAS":
            return {
                ...state,
                meta: action.payload,
            };
        case "entities/SET_FORM_SCHEMA":
            return {
                ...state,
                form: {
                    ...state.form,
                    [action.payload.entityAlias]: action.payload.form,
                },
            };
        case "entities/SET_GRID_SCHEMA":
            return {
                ...state,
                grid: {
                    ...state.grid,
                    [action.payload.entityAlias]: action.payload.grid,
                },
            };
        default:
            return state;
    }
};

export default entitiesReducer;
