import { ActionTypes as EntitiesActionTypes, IState as IEntitiesState } from "../redux/entities/actions";
import { ActionTypes as UserActionTypes, IState as IUserState } from "../redux/user/actions";

export interface IReduxState {
    entities: IEntitiesState;
    user: IUserState;
}

export type ActionTypes = EntitiesActionTypes | UserActionTypes;
