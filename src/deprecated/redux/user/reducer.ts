import { ActionTypes, IState } from "./actions";

const initialState: IState = {};

const reducer = (state: IState = initialState, action: ActionTypes): IState => {
    switch (action.type) {
        case "user/LOGOUT":
            return {
                ...state,
                loginLoading: undefined,
                user: undefined,
            };
        case "user/PERSIST":
            return {
                ...state,
                ...action.payload,
            };
        default:
            return state;
    }
};

export default reducer;
