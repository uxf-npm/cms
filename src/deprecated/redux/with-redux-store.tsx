import React from "react";
import { Provider } from "react-redux";
import { Store } from "redux";
import { initializeStore } from "./configureStore";
import { IReduxState } from "./types";

const isServer = typeof window === "undefined";
const __NEXT_REDUX_STORE__ = "__NEXT_REDUX_STORE__";

const getOrCreateStore = (initialState: Partial<IReduxState> = {}): Store<IReduxState> => {
    // Always make a new store if server, otherwise state is shared between requests
    if (isServer) {
        return initializeStore(initialState);
    }

    // Create store if unavailable on the client and set it on the window object
    if (!(window as any)[__NEXT_REDUX_STORE__]) {
        (window as any)[__NEXT_REDUX_STORE__] = initializeStore(initialState);
    }
    return (window as any)[__NEXT_REDUX_STORE__];
};

export const withReduxStore = (App: any): any => {
    return class AppWithRedux extends React.Component {
        private readonly reduxStore: Store<IReduxState>;

        public static async getInitialProps(appContext: any) {
            // Get or Create the store with `undefined` as initialState
            // This allows you to set a custom default initialState
            const reduxStore = getOrCreateStore();

            // Provide the store to getInitialProps of pages
            appContext.ctx.reduxStore = reduxStore;

            let appProps = {};
            if (typeof App.getInitialProps === "function") {
                appProps = await App.getInitialProps(appContext);
            }

            return {
                ...appProps,
                initialReduxState: reduxStore.getState(),
            };
        }

        constructor(props: any) {
            super(props);
            this.reduxStore = getOrCreateStore(props.initialReduxState);
        }

        public render(): JSX.Element {
            return (
                <Provider store={this.reduxStore}>
                    <App {...this.props} />
                </Provider>
            );
        }
    };
};
