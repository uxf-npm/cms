import { ForbiddenError } from "../../errors/ForbiddenError";
import { NetworkError } from "../../errors/NetworkError";
import { UnauthorizedError } from "../../errors/UnauthorizedError";
import axios, { AxiosError, AxiosInstance, AxiosRequestConfig } from "axios";
import { NextPageContext } from "next";

export type ContextType = NextPageContext | null;

const getClient = (options: AxiosRequestConfig = {}) => {
    const client = axios.create(options);

    // Add a request interceptor
    client.interceptors.request.use(
        (requestConfig) => requestConfig,
        (requestError) => {
            // eslint-disable-next-line no-console
            console.error(requestError);

            return Promise.reject(requestError);
        },
    );

    // Add a response interceptor
    client.interceptors.response.use(
        (response) => response,
        (error) => {
            if (error.response && error.response.status >= 500) {
                // eslint-disable-next-line no-console
                console.error(error);
            }

            return Promise.reject(error);
        },
    );

    return client;
};

const addCookiesToHeader = (ctx: ContextType, headers: object = {}) => {
    return ctx?.req?.headers.cookie ? { ...headers, cookie: ctx.req.headers.cookie } : headers;
};

const handleError = (error: AxiosError) => {
    if (error.response) {
        if (error.response.status === 401) {
            throw new UnauthorizedError();
        } else if (error.response.status === 403) {
            throw new ForbiddenError();
        } else {
            throw error;
        }
    } else if (error.request) {
        throw new NetworkError();
    } else {
        throw error;
    }
};

/** @deprecated */
export default class ApiClient {
    private client: AxiosInstance;

    constructor(basePath = "", baseUrl: string = "") {
        this.client = getClient({
            baseURL: `${baseUrl}${basePath}`,
        });
    }

    private request<RESPONSE = any>(ctx: ContextType, config: AxiosRequestConfig) {
        return this.client
            .request<RESPONSE>({
                ...config,
                headers: addCookiesToHeader(ctx, config.headers),
            })
            .then((response) => Promise.resolve(response))
            .catch((error: AxiosError) => handleError(error));
    }

    public get<RESPONSE = any>(ctx: ContextType, url: string, config: AxiosRequestConfig = {}) {
        return this.request<RESPONSE>(ctx, { ...config, url, method: "GET" });
    }

    public delete<RESPONSE = any>(ctx: ContextType, url: string, config: AxiosRequestConfig = {}) {
        return this.request<RESPONSE>(ctx, { ...config, url, method: "DELETE" });
    }

    public head<RESPONSE = any>(ctx: ContextType, url: string, config: AxiosRequestConfig = {}) {
        return this.request<RESPONSE>(ctx, { ...config, url, method: "HEAD" });
    }

    public post<REQUEST extends object = {}, RESPONSE = any>(
        ctx: ContextType,
        url: string,
        data: REQUEST,
        config: AxiosRequestConfig = {},
    ) {
        return this.request<RESPONSE>(ctx, { ...config, url, method: "POST", data });
    }

    public put<REQUEST extends object = {}, RESPONSE = any>(
        ctx: ContextType,
        url: string,
        data: REQUEST,
        config: AxiosRequestConfig = {},
    ) {
        return this.request<RESPONSE>(ctx, { ...config, url, method: "PUT", data });
    }

    public patch<REQUEST extends object = {}, RESPONSE = any>(
        ctx: ContextType,
        url: string,
        data: REQUEST,
        config: AxiosRequestConfig = {},
    ) {
        return this.request<RESPONSE>(ctx, { ...config, url, method: "PATCH", data });
    }
}
