import { Schema } from "@uxf/data-grid";
import { createAxiosInstance } from "../lib/api";
import { FileResponse } from "../ui/FileUploadInput";

const { axiosRequest, axiosInstance } = createAxiosInstance();

export type VisibilityLevel = "PUBLIC" | "PUBLIC_WITHOUT_SITEMAP" | "PRIVATE"; // TODO duplicated in ContentBuilder

export interface ContentSeoResponse {
    name: string | null;
    title: string;
    description: string;
    ogTitle: string;
    ogDescription: string;
    ogImage: FileResponse | null;
}

export interface ContentAuthorResponse {
    id: number;
    firstName: string;
    surname: string;
    avatar: FileResponse | null;
}

export interface ContentCategoryResponse {
    id: number;
    name: string;
}

export interface ContentTagResponse {
    id: number;
    code: string;
    label: string;
}

export interface ContentResponse {
    id: number;
    type: string;
    name: string;
    perex: string;
    seo: ContentSeoResponse;
    visibilityLevel: VisibilityLevel;
    hidden: boolean;
    image: FileResponse | null;
    publishedAt: string | null;
    author: ContentAuthorResponse | null;
    category: ContentCategoryResponse | null;
    content: any[];
    tags: ContentTagResponse[];
    createdAt: string;
    updatedAt: string;
}

export interface ContentSeoRequestBody {
    name: string | null;
    title: string;
    description: string;
    ogTitle: string;
    ogDescription: string;
    ogImage: number | null;
}

export interface ContentRequestBody {
    type: string;
    name: string;
    perex: string;
    seo: ContentSeoRequestBody;
    visibilityLevel: VisibilityLevel;
    hidden: boolean;
    image: number | null;
    publishedAt: string | null;
    author: number | null;
    category: number | null;
    content: any;
    tags: number[];
}

export interface FieldSchemaResponse {
    autocomplete: string | null;
    name: string;
    type: string;
    label: string;
    required: boolean;
    readOnly: boolean;
    editable: boolean;
    fields: FieldSchemaResponse[];
    options: Array<{ id: string; label: string }> | null;
}

export interface FormSchemaResponse {
    fields: FieldSchemaResponse[];
}

export default interface UserResponse {
    id: number | string;
    email: string;
    name: string;
    roles: string[];
}

export interface UserInfoResponse {
    user: UserResponse;
}

export interface EntityMetaSchema {
    title: string;
    entityAlias: string;
    actions: string[];
}

export interface EntityMetaSchemasResponse {
    [entityAlias: string]: EntityMetaSchema;
}

export interface AutocompleteResultResponse {
    id: number | string;
    label: string;
}

export interface LoginRequestBody {
    username: string;
    password: string;
}

export interface LoginResponse {
    access_token: string;
    refresh_token: string;
    expires_in: number;
    token_type: string;
}

export interface UserConfigResponse<T extends any = any> {
    id: number;
    name: string;
    data: T;
}

export interface UserConfigBagResponse {
    [name: string]: UserConfigResponse;
}

export function contentGet(ctx: any, contentId: number) {
    return axiosRequest<ContentResponse>(ctx, `/api/cms/content/${contentId}`, "get", null, null);
}

export function contentCreate(ctx: any, content: ContentRequestBody) {
    return axiosRequest<ContentResponse>(ctx, "/api/cms/content", "post", content, null);
}

export function contentUpdate(ctx: any, contentId: number, content: ContentRequestBody) {
    return axiosRequest<ContentResponse>(ctx, `/api/cms/content/${contentId}`, "put", content, null);
}

export function dataGridSchemaGet(ctx: any, gridName: string) {
    return axiosRequest<Schema<any>>(ctx, `/api/cms/datagrid/schema/${gridName}`, "get", null, null);
}

export const dataGridAutocompleteGet = (ctx: any, gridName: string, filterName: string, text: string) => {
    return axiosRequest<Array<{ value: number; label: string }>>(
        ctx,
        `/api/cms/datagrid/autocomplete/${gridName}/${filterName}`,
        "get",
        null,
        { s: text },
    ).then((r) => r.data);
};

export function forgottenPassword(ctx: any, email: string, cms = true): Promise<void> {
    return axiosRequest(ctx, "/api/forgotten-password", "post", { email, cms }, null).then(() => undefined);
}

export function getLoggedUser(ctx: any = null) {
    return axiosRequest<UserInfoResponse>(ctx, `/api/cms/user`, "get", null, null).then((r) => r.data.user);
}

export function getEntityMetaSchemas(ctx: any) {
    return axiosRequest<EntityMetaSchemasResponse>(ctx, "/api/cms/tables", "get", null, null);
}

export function getFormSchema(ctx: any, path: { entityAlias: string }) {
    return axiosRequest<FormSchemaResponse>(ctx, `/api/cms/form/${path.entityAlias}/schema`, "get", null, null);
}

export function getFormValues(ctx: any, path: { entityAlias: string; id: number }) {
    return axiosRequest<any>(ctx, `/api/cms/form/${path.entityAlias}/${path.id}`, "get", null, null);
}

export function login(ctx: any, body: LoginRequestBody) {
    return axiosRequest<LoginResponse>(ctx, "/api/login", "post", body, null);
}

export function autocomplete(ctx: any, path: { name: string }, query: { term: string }) {
    return axiosRequest<AutocompleteResultResponse[]>(ctx, `/api/cms/autocomplete/${path.name}`, "get", null, query);
}

export function saveFormValues<T extends {}>(ctx: any, path: { entityAlias: string; id?: number | null }, body: T) {
    return path.id
        ? axiosRequest<T>(ctx, `/api/cms/form/${path.entityAlias}/${path.id}`, "put", body, null)
        : axiosRequest<T>(ctx, `/api/cms/form/${path.entityAlias}`, "post", body, null);
}

export async function userConfigSave(name: string, data: any) {
    return axiosRequest<UserConfigBagResponse>(null, "/api/cms/user-config", "post", { name, data }, null);
}

export function userConfigGetAll(ctx: any) {
    return axiosRequest<UserConfigBagResponse>(ctx, "/api/cms/user-config", "get", null, null);
}

export async function userConfigGet<T extends any = any>(ctx: any, name: string, defaultValue?: T): Promise<T> {
    const configs = await userConfigGetAll(ctx).then((r) => r.data);
    return configs[name]?.data || defaultValue;
}

export async function uploadFile(file: File, namespace: string | null = null): Promise<FileResponse> {
    const formData = new FormData();
    formData.append("files[]", file);

    const response = await axiosInstance.post<FileResponse[]>("/api/cms/files/upload", formData, {
        timeout: 100 * 1000,
        params: namespace ? { namespace } : {},
    });

    if (response.data.length > 0) {
        return response.data[0];
    }

    throw new Error("Response is null");
}
