import { ReactNode } from "react";
import { NextPageContext } from "next";
import UserResponse, { uploadFile, userConfigGet } from "../api";
import { FileResponse } from "../ui/FileUploadInput";
import { FlashMessagesService } from "../services/FlashMessagesService";
import { LinkProps } from "next/link";
import { FunctionParametersGenerator } from "@uxf/router";

export interface NotificationServiceInterface {
    info: (message: ReactNode) => void;
    success: (message: ReactNode) => void;
    warning: (message: ReactNode) => void;
    error: (message: ReactNode) => void;
}

export interface ErrorServiceInterface {
    handleError: (e: any) => void;
    logError: (e: any) => void;
}

export interface LoggedUser {
    roles: string[];
}

export type UseLoggedUser = () => LoggedUser;
export type GetLoggedUser = (ctx: NextPageContext) => Promise<UserResponse>;
export type GetUserConfig = (ctx: NextPageContext, name: string, defaultValue: any) => Promise<any>;
export type UploadFile = (file: File, namespace?: string) => Promise<FileResponse>;

type ContainerStore = {
    route: (...attr: FunctionParametersGenerator<any>) => LinkProps["href"];
    useActiveRoute: () => string;
    useLoggedUser: UseLoggedUser;
    "api.getLoggedUser": GetLoggedUser;
    "api.getUserConfig": GetUserConfig;
    "api.uploadFile": UploadFile;
    "service.notification": NotificationServiceInterface;
    "service.error": ErrorServiceInterface;
};

export class Container {
    // TODO @vejvis - move initial values to better place
    private store: Partial<ContainerStore> = {
        "api.uploadFile": uploadFile,
        "api.getUserConfig": userConfigGet,
        "service.notification": FlashMessagesService,
    };

    public register<T extends keyof ContainerStore>(key: T, value: ContainerStore[T]): Container {
        this.store[key] = value;
        return this;
    }

    public get<T extends keyof ContainerStore>(key: T): ContainerStore[T] {
        const value = this.store[key];

        if (!value) {
            throw new Error(`Container has no service '${key}'.`);
        }

        return value as any;
    }

    public has(key: keyof ContainerStore): boolean {
        return !!this.store[key];
    }
}
