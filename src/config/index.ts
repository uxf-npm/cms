import { Config } from "./Config";
import { Container } from "./Container";

export const config = new Config();

export const container = new Container();
