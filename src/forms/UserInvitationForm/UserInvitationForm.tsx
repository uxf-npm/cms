import Grid from "@mui/material/Grid";
import React, { ReactElement } from "react";
import { Form } from "react-final-form";
import { AutocompleteMultipleOption } from "../../ui/AutocompleteMultiple";
import { Button } from "../../ui/Button";
import { AutocompleteMultiple } from "../components/AutocompleteMultiple";
import { TextInput } from "../components/TextInput";
import { UserInvitationFormData, UserInvitationFormProps } from "./types";

export const UserInvitationForm = (props: UserInvitationFormProps): ReactElement => {
    const { onLoadRoles, ...formProps } = props;

    return (
        <Form<UserInvitationFormData> {...formProps}>
            {({ handleSubmit, submitting }) => (
                <form onSubmit={handleSubmit}>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <TextInput<UserInvitationFormData> label="Jméno" name="name" required />
                        </Grid>
                        <Grid item xs={12}>
                            <TextInput<UserInvitationFormData> label="Příjmení" name="surname" required />
                        </Grid>
                        <Grid item xs={12}>
                            <TextInput<UserInvitationFormData> label="E-mail" name="email" required />
                        </Grid>
                        <Grid item xs={12}>
                            <AutocompleteMultiple<UserInvitationFormData, AutocompleteMultipleOption>
                                name="roles"
                                label="Role"
                                required
                                loadOptions={onLoadRoles}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Button
                                color="primary"
                                disabled={submitting}
                                loading={submitting}
                                type="submit"
                                variant="contained"
                            >
                                Vytvořit uživatele a poslat mu pozvánku
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            )}
        </Form>
    );
};
