import { FormProps } from "react-final-form";
import { AutocompleteOption } from "../../ui/Autocomplete";
import { AutocompleteProps } from "../components/Autocomplete";

export interface UserInvitationFormData {
    name: string;
    surname: string;
    email: string;
    roles: AutocompleteOption[];
}

export interface UserInvitationFormProps extends FormProps<UserInvitationFormData> {
    onLoadRoles: AutocompleteProps<any, AutocompleteOption>["loadOptions"];
}
