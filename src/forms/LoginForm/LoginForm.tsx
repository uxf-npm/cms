import React, { useEffect, useState } from "react";
import { Button } from "../../ui/Button/Button";
import { TextInput } from "../components/TextInput";
import { Form } from "react-final-form";
import Grid from "@mui/material/Grid";
import { LoginFormProps } from "./types";

// TODO: email validation

export const LoginForm: React.FC<LoginFormProps> = (props) => {
    const { onForgottenPassword, ...formProps } = props;

    const [formDisabled, setFormDisabled] = useState(true);

    useEffect(() => {
        setFormDisabled(false);
    }, []);

    return (
        <Form {...formProps}>
            {({ handleSubmit, submitting }) => (
                <form onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextInput disabled={formDisabled} label="E-mail" name="username" required type="text" />
                        </Grid>
                        <Grid item xs={12}>
                            <TextInput disabled={formDisabled} label="Heslo" name="password" required type="password" />
                        </Grid>
                        <Grid item xs={12}>
                            <Button
                                color="primary"
                                disabled={submitting}
                                fullWidth
                                loading={submitting}
                                type="submit"
                                variant="contained"
                                mt={2}
                            >
                                Přihlásit
                            </Button>
                        </Grid>
                        {onForgottenPassword && (
                            <Grid item xs={12}>
                                <Button size="small" onClick={onForgottenPassword} disabled={submitting} fullWidth>
                                    Zapomenuté heslo
                                </Button>
                            </Grid>
                        )}
                    </Grid>
                </form>
            )}
        </Form>
    );
};
