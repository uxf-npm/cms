import { FormProps } from "react-final-form";

export interface RenewPasswordFormData {
    password: string;
    passwordAgain: string;
}

export type RenewPasswordFormProps = FormProps<RenewPasswordFormData>;
