import { FieldValidator } from "final-form";
import React, { FocusEventHandler, ReactElement, ReactNode, useCallback } from "react";
import { useField } from "react-final-form";
import { Select as UISelect } from "../../../ui/Select";
import { SelectProps } from "./types";

const requiredValidator =
    (errorMessage: ReactNode): FieldValidator<any> =>
    (value: string) =>
        value ? undefined : errorMessage;

export const Select = <FormData extends Record<string, any>, T extends string | number>(
    props: SelectProps<FormData, T>,
): ReactElement => {
    const {
        label,
        name,
        onBlur,
        onChange,
        onFocus,
        options,
        required,
        requiredErrorMessage = "Vyplňte políčko",
        error,
        ...selectProps
    } = props;

    const { input, meta } = useField<T>(name as string, {
        allowNull: true,
        validate: required ? requiredValidator(requiredErrorMessage) : undefined,
    });

    const onBlurHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onBlur(e);
            if (onBlur) {
                onBlur(e);
            }
        },
        [input, onBlur],
    );

    const onFocusHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onFocus(e);
            if (onFocus) {
                onFocus(e);
            }
        },
        [input, onFocus],
    );

    return (
        <UISelect
            {...selectProps}
            label={required ? `${label} *` : label}
            error={error || (meta.error && meta.touched)}
            value={input.value ?? ""}
            onChange={(event, child) => {
                input.onChange(event.target.value);
                if (onChange) {
                    onChange(event, child);
                }
            }}
            helperText={meta.error && meta.touched ? meta.error : undefined}
            options={options}
            onBlur={onBlurHandler}
            onFocus={onFocusHandler}
        />
    );
};
