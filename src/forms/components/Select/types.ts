import { ReactNode } from "react";
import { SelectProps as UISelectProps } from "../../../ui/Select";

export type SelectProps<FormData extends Record<string, any>, T extends string | number> = Omit<
    UISelectProps<T>,
    "value" | "name"
> & {
    name: keyof FormData;
    onChange?: UISelectProps<T>["onChange"];
    required?: boolean;
    requiredErrorMessage?: ReactNode;
};
