import TextField from "@mui/material/TextField";
import { FieldValidator } from "final-form";
import React, { FocusEventHandler, ReactNode, useCallback } from "react";
import { useField } from "react-final-form";
import { composeValidators } from "../../../utils/composeValidators";
import { TextInputProps } from "./types";

const requiredValidator =
    (errorMessage: ReactNode): FieldValidator<string> =>
    (value: string) =>
        value === "" || value === null || value === undefined ? errorMessage : undefined;

export const TextInput = <FormData extends Record<string, any>>(props: TextInputProps<FormData>) => {
    const {
        disabled,
        error,
        helperText,
        label,
        name,
        onBlur,
        onFocus,
        required,
        requiredErrorMessage = "Vyplňte políčko",
        validate = [],
        ...restProps
    } = props;

    const validator = composeValidators(
        required && !disabled ? requiredValidator(requiredErrorMessage) : undefined,
        ...validate,
    );

    const { input, meta } = useField<string | null>(name as string, {
        validate: validator,
        allowNull: true,
        parse: (value) => (value === "" ? null : value),
    });

    const onBlurHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onBlur(e);
            if (onBlur) {
                onBlur(e);
            }
        },
        [input, onBlur],
    );

    const onFocusHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onFocus(e);
            if (onFocus) {
                onFocus(e);
            }
        },
        [input, onFocus],
    );

    return (
        <TextField
            error={error || !!(meta.touched && meta.error)}
            helperText={(meta.touched && meta.error) || helperText}
            disabled={disabled}
            fullWidth
            variant="outlined"
            size="small"
            label={
                <>
                    {label}
                    {required ? " *" : ""}
                </>
            }
            name={name as string}
            onChange={input.onChange}
            value={input.value ?? ""}
            onBlur={onBlurHandler}
            onFocus={onFocusHandler}
            InputLabelProps={{ shrink: true, ...(restProps.InputLabelProps ?? {}) }}
            {...restProps}
        />
    );
};
