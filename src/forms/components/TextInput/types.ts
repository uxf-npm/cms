import { TextFieldProps } from "@mui/material";
import { FieldValidator } from "final-form";
import { ReactNode } from "react";

export interface TextInputProps<FormData extends Record<string, any>>
    extends Omit<TextFieldProps, "name" | "onChange" | "value"> {
    helperText?: ReactNode;
    name: keyof FormData;
    requiredErrorMessage?: ReactNode;
    validate?: FieldValidator<string>[];
}
