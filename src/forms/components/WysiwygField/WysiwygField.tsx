import { WysiwygContent } from "@uxf/wysiwyg";
import { Toolbar } from "@uxf/wysiwyg-mui5-plugins";
import { EditorPlugin, WysiwygEditor } from "@uxf/wysiwyg/Editor";
import React, { FC } from "react";
import { useField } from "react-final-form";
import { DEFAULT_EDITOR_PLUGINS } from "../../../pages/ContentBuilder/content/Wysiwyg";

interface Props {
    name: string;
    editorPlugins?: EditorPlugin[];
}

export const WysiwygField: FC<Props> = ({ name, editorPlugins }) => {
    const { input } = useField<WysiwygContent>(name, {
        allowNull: true,
    });
    const plugins: EditorPlugin[] = editorPlugins ?? DEFAULT_EDITOR_PLUGINS;

    return (
        <WysiwygEditor
            value={input.value}
            onChange={input.onChange}
            Toolbar={Toolbar}
            plugins={plugins}
            stylesForEditable={{
                border: "1px solid #e1e1e1",
                padding: 12,
                borderBottomLeftRadius: 4,
                borderBottomRightRadius: 4,
                borderTop: 0,
            }}
        />
    );
};
