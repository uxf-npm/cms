import { ReactNode } from "react";

export interface CheckboxProps<FormData extends Record<string, any>> {
    disabled?: boolean;
    helperText?: ReactNode;
    id?: string;
    label?: ReactNode;
    name: keyof FormData;
    required?: boolean;
}
