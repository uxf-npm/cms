import Box from "@mui/material/Box";
import MuiCheckbox from "@mui/material/Checkbox";
import FormControl from "@mui/material/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormHelperText from "@mui/material/FormHelperText";
import React, { ReactElement } from "react";
import { useField } from "react-final-form";
import { CheckboxProps } from "./types";

export const Checkbox = <FormData extends Record<string, any>>(props: CheckboxProps<FormData>): ReactElement => {
    const { name, id, label, disabled, required, helperText } = props;
    const { input } = useField(name as string, { type: "checkbox" });

    return (
        <FormControl>
            <FormControlLabel
                label={
                    <>
                        {label}
                        {required ? " *" : ""}
                    </>
                }
                disabled={disabled}
                htmlFor={id}
                control={
                    <MuiCheckbox
                        onChange={input.onChange}
                        checked={input.checked}
                        required={required}
                        disabled={disabled}
                        id={id}
                        onBlur={input.onBlur}
                        onFocus={input.onFocus}
                    />
                }
            />
            {helperText && (
                <Box mt={-1}>
                    <FormHelperText>{helperText}</FormHelperText>
                </Box>
            )}
        </FormControl>
    );
};
