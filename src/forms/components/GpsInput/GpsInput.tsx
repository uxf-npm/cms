import TextField, { TextFieldProps } from "@mui/material/TextField";
import { FieldValidator } from "final-form";
import React, { ReactNode, useCallback, useEffect, useState } from "react";
import { useField } from "react-final-form";
import { composeValidators } from "../../../utils/composeValidators";
import { Gps, GpsInputProps } from "./types";
import Tooltip from "@mui/material/Tooltip";
import Coordinates from "coordinate-parser";

const requiredValidator =
    (errorMessage: ReactNode): FieldValidator<Gps> =>
    (value) =>
        value === null || value === undefined ? errorMessage : undefined;

const SUPPORTED_FORMATS = (
    <div>
        40.123, -74.123
        <br />
        40.123° N 74.123° W<br />
        40° 7´ 22.8&quot; N 74° 7´ 22.8&quot; W<br />
        40° 7.38’ , -74° 7.38’
        <br />
        N40°7’22.8, W74°7’22.8&quot;
        <br />
        40°7’22.8&quot;N, 74°7’22.8&quot;W
        <br />
        40 7 22.8, -74 7 22.8
        <br />
        40.123 -74.123
        <br />
        40.123°,-74.123°
        <br />
        144442800, -266842800
        <br />
        40.123N74.123W
        <br />
        4007.38N7407.38W
        <br />
        40°7’22.8&quot;N, 74°7’22.8&quot;W
        <br />
        400722.8N740722.8W
        <br />
        N 40 7.38 W 74 7.38
        <br />
        40:7:23N,74:7:23W
        <br />
        40:7:22.8N 74:7:22.8W
        <br />
        40°7’23&quot;N 74°7’23&quot;W
        <br />
        40°7’23&quot; -74°7’23&quot;
        <br />
        40d 7’ 23&quot; N 74d 7’ 23&quot; W<br />
        40.123N 74.123W
        <br />
        40° 7.38, -74° 7.38
        <br />
    </div>
);
const EMPTY_HELPER_TEXT = (
    <>
        Zadejte souřadnice (
        <Tooltip title={SUPPORTED_FORMATS}>
            <span style={{ textDecoration: "underline", cursor: "pointer" }}>podporované formáty</span>
        </Tooltip>
        )
    </>
);

export const GpsInput = <FormData extends Record<string, any>>(props: GpsInputProps<FormData>) => {
    const {
        disabled,
        error,
        label,
        name,
        required,
        requiredErrorMessage = "Vyplňte GPS souřadnice",
        validate = [],
        ...restProps
    } = props;

    const validator = composeValidators(
        required && !disabled ? requiredValidator(requiredErrorMessage) : undefined,
        ...validate,
    );

    const { input, meta } = useField<Gps | null>(name as string, {
        validate: validator,
        allowNull: true,
        parse: (value) => (value === null ? null : value),
    });

    const [fieldValue, setFieldValue] = useState(input.value ? `${input.value.lat}N, ${input.value.lng}E` : "");

    useEffect(() => {
        if (input.value && fieldValue === "") {
            setFieldValue(`${input.value.lat}N, ${input.value.lng}E`);
        }
    }, [input.value, fieldValue]);

    const handleChange = useCallback<NonNullable<TextFieldProps["onChange"]>>(
        (e) => {
            setFieldValue(e.target.value);
            try {
                const coords = new Coordinates(e.target.value);
                input.onChange({ lat: coords.getLatitude(), lng: coords.getLongitude() });
            } catch (e) {
                input.onChange(null);
            }
        },
        [input],
    );

    return (
        <TextField
            error={error || !!(meta.touched && meta.error)}
            helperText={
                meta.touched && meta.error ? (
                    `${meta.error}`
                ) : input.value ? (
                    `${input.value.lat}N, ${input.value.lng}E`
                ) : fieldValue ? (
                    <>
                        Souřadnice nejsou validní (
                        <Tooltip title={SUPPORTED_FORMATS}>
                            <span style={{ textDecoration: "underline", cursor: "pointer" }}>podporované formáty</span>
                        </Tooltip>
                        )
                    </>
                ) : (
                    EMPTY_HELPER_TEXT
                )
            }
            disabled={disabled}
            fullWidth
            variant="outlined"
            size="small"
            label={
                <>
                    {label}
                    {required ? " *" : ""}
                </>
            }
            name={name as string}
            onChange={handleChange}
            value={fieldValue}
            {...restProps}
        />
    );
};
