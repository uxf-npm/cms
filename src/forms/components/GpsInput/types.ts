import { TextFieldProps } from "@mui/material/TextField";
import { FieldValidator } from "final-form";
import { ReactNode } from "react";

export type Gps = {
    lat: number;
    lng: number;
};

export interface GpsInputProps<FormData extends Record<string, any>>
    extends Omit<TextFieldProps, "name" | "onChange" | "value" | "helperText"> {
    helperText?: ReactNode;
    name: keyof FormData;
    requiredErrorMessage?: ReactNode;
    validate?: FieldValidator<Gps>[];
}
