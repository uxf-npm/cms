import { FieldValidator } from "final-form";
import { ReactNode } from "react";
import { NumberInputProps as UINumberInputProps } from "../../../ui/NumberInput";

export interface NumberInputProps<FormData extends Record<string, any>>
    extends Omit<UINumberInputProps, "name" | "onChange" | "value"> {
    name: keyof FormData;
    requiredErrorMessage?: ReactNode;
    validate?: FieldValidator<string>[];
}
