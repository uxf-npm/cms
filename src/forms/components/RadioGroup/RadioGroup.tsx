import { FormControl, FormControlLabel, FormHelperText, FormLabel, Radio } from "@mui/material";
import MuiRadioGroup from "@mui/material/RadioGroup";
import { FieldValidator } from "final-form";
import React, { ChangeEvent, FocusEventHandler, ReactElement, ReactNode, useCallback } from "react";
import { useField } from "react-final-form";
import { RadioGroupProps } from "./types";

const requiredValidator =
    (errorMessage: ReactNode): FieldValidator<any> =>
    (value: string) =>
        value ? undefined : errorMessage;

export const RadioGroup = <FormData extends Record<string, any>>(props: RadioGroupProps<FormData>): ReactElement => {
    const {
        disabled,
        error,
        helperText,
        label,
        name,
        onBlur,
        onChange,
        onFocus,
        options = [],
        required,
        requiredErrorMessage = "Vyberte možnost",
        ...radioGroupProps
    } = props;

    const { input, meta } = useField<string | null>(name as string, {
        allowNull: true,
        defaultValue: null,
        validate: required ? requiredValidator(requiredErrorMessage) : undefined,
    });

    const onChangeHandler = useCallback(
        (event: ChangeEvent<HTMLInputElement>, value: string) => {
            input.onChange(value ?? null);
            if (onChange) {
                onChange(event, value);
            }
        },
        [input, onChange],
    );

    const onBlurHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onBlur(e);
            if (onBlur) {
                onBlur(e);
            }
        },
        [input, onBlur],
    );

    const onFocusHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onFocus(e);
            if (onFocus) {
                onFocus(e);
            }
        },
        [input, onFocus],
    );

    const helperTextOrError = meta.error && meta.touched ? meta.error : helperText;
    const hasFormError = meta.error && meta.touched;

    return (
        <FormControl component="fieldset" error={error || !!hasFormError}>
            <FormLabel component="legend">{required ? `${label} *` : label}</FormLabel>
            <MuiRadioGroup
                {...radioGroupProps}
                value={input.value}
                onChange={onChangeHandler}
                onBlur={onBlurHandler}
                onFocus={onFocusHandler}
            >
                {options.map((option) => (
                    <FormControlLabel
                        key={option.id}
                        value={option.id}
                        control={<Radio />}
                        label={option.label}
                        disabled={disabled || option.disabled}
                    />
                ))}
            </MuiRadioGroup>
            {!!helperTextOrError && <FormHelperText>{helperTextOrError}</FormHelperText>}
        </FormControl>
    );
};
