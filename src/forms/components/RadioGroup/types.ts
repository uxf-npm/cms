import { RadioGroupProps as MuiRadioGroupProps } from "@mui/material/RadioGroup";
import React, { ReactNode } from "react";

export type RadioGroupOption = { id: string | number; label: string | number | React.ReactElement; disabled?: boolean };

export type RadioGroupProps<FormData extends Record<string, any>> = Omit<MuiRadioGroupProps, "value" | "name"> & {
    disabled?: boolean;
    error?: boolean;
    helperText?: string;
    label: string;
    name: keyof FormData;
    options: RadioGroupOption[];
    required?: boolean;
    requiredErrorMessage?: ReactNode;
};
