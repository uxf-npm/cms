import { FileResponse, FileUploadInput as UIInput } from "../../../ui/FileUploadInput";
import { FileUploadInputProps } from "./types";
import React from "react";
import { useField } from "react-final-form";
import { FieldValidator } from "final-form";

const requiredValidator: FieldValidator<any> = (v) => (v === null ? "Nahrajte soubor" : undefined);

export const FileUploadInput: React.FC<FileUploadInputProps> = (props) => {
    const { required, ...inputProps } = props;
    const { input, meta } = useField<FileResponse | null>(props.name, {
        allowNull: true,
        validate: required ? requiredValidator : undefined,
    });
    return <UIInput {...inputProps} error={meta.touched && meta.error} onChange={input.onChange} value={input.value} />;
};
