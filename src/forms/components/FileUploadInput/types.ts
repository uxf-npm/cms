import { FileUploadInputProps as UIProps } from "../../../ui/FileUploadInput";

export interface FileUploadInputProps extends Omit<UIProps, "value" | "onChange"> {
    required?: boolean;
}
