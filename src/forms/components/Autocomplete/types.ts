import { AutocompleteOption, AutocompleteProps as UIAutocompleteProps } from "../../../ui/Autocomplete";
import { ReactNode } from "react";

export type AutocompleteProps<
    FormData extends Record<string, any>,
    T extends AutocompleteOption,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined,
> = Omit<UIAutocompleteProps<T, DisableClearable, FreeSolo>, "onChange" | "name" | "value" | "multiple"> & {
    name: keyof FormData;
    onChange?: UIAutocompleteProps<T, DisableClearable, FreeSolo>["onChange"];
    required?: boolean;
    requiredErrorMessage?: ReactNode;
};
