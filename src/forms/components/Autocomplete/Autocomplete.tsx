import { FieldValidator } from "final-form";
import { Autocomplete as UIAutocomplete, AutocompleteOption } from "../../../ui/Autocomplete";
import React, { FocusEventHandler, ReactElement, ReactNode, useCallback } from "react";
import { useField } from "react-final-form";
import { AutocompleteProps } from "./types";

const requiredValidator =
    (errorMessage: ReactNode): FieldValidator<any> =>
    (value: string) =>
        value ? undefined : errorMessage;

export const Autocomplete = <
    FormData extends Record<string, any>,
    T extends AutocompleteOption,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined,
>(
    props: AutocompleteProps<FormData, T, DisableClearable, FreeSolo>,
): ReactElement => {
    const {
        label,
        name,
        onBlur,
        onChange,
        onFocus,
        required,
        requiredErrorMessage = "Vyplňte políčko",
        TextFieldProps = {},
        ...restProps
    } = props;
    const { input, meta } = useField(name as string, {
        allowNull: true,
        defaultValue: null,
        validate: required ? requiredValidator(requiredErrorMessage) : undefined,
    });

    const onBlurHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onBlur(e);
            if (onBlur) {
                onBlur(e);
            }
        },
        [input, onBlur],
    );

    const onFocusHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onFocus(e);
            if (onFocus) {
                onFocus(e);
            }
        },
        [input, onFocus],
    );

    return (
        <UIAutocomplete
            value={input.value}
            onChange={(event, value, reason, details) => {
                input.onChange(value);
                if (onChange) {
                    onChange(event, value, reason, details);
                }
            }}
            label={required ? `${label} *` : label}
            TextFieldProps={{
                ...TextFieldProps,
                helperText: meta.error && meta.touched ? meta.error : undefined,
                error: TextFieldProps.error || (meta.error && meta.touched),
            }}
            onBlur={onBlurHandler}
            onFocus={onFocusHandler}
            {...restProps}
        />
    );
};
