import {
    DateTimePicker as MuiDateTimePicker,
    DateTimePickerProps as MuiDateTimePickerProps,
} from "@mui/x-date-pickers/DateTimePicker";
import { TextField } from "@mui/material";
import { FieldValidator } from "final-form";
import React, { ReactElement, ReactNode, useCallback } from "react";
import { useField } from "react-final-form";
import { composeValidators } from "../../../utils/composeValidators";
import { DateTimePickerProps } from "./types";
import { Dayjs } from "dayjs";

const requiredValidator =
    (errorMessage: ReactNode): FieldValidator<string> =>
    (value: string) =>
        value ? undefined : errorMessage;

export const DateTimePicker = <FormData extends Record<string, any>>(
    props: DateTimePickerProps<FormData>,
): ReactElement => {
    const {
        disabled,
        error,
        label,
        name,
        required,
        requiredErrorMessage = "Vyplňte políčko",
        validate = [],
        variant = "outlined",
        mask,
        // TODO rest props
    } = props;

    const validator = composeValidators(
        required && !disabled ? requiredValidator(requiredErrorMessage) : undefined,
        ...validate,
    );

    const { input, meta } = useField<string | null>(name as string, {
        allowNull: true,
        validate: validator,
    });

    const onChange = useCallback<MuiDateTimePickerProps<Dayjs>["onChange"]>(
        (date) => {
            if (date?.isValid()) {
                input.onChange(date?.format());
            } else if (!date) {
                input.onChange(null);
            }
        },
        [input],
    );

    return (
        <MuiDateTimePicker
            clearable
            renderInput={(props) => (
                <TextField
                    {...props}
                    variant={variant}
                    size="small"
                    helperText={meta.touched && meta.error}
                    error={error || !!(meta.touched && meta.error)}
                    onFocus={input.onFocus}
                    onBlur={input.onBlur}
                    fullWidth
                />
            )}
            ampm={false}
            label={
                <>
                    {label}
                    {required ? " *" : ""}
                </>
            }
            mask={mask ?? "__.__.____ __:__"}
            inputFormat="DD.MM.YYYY HH:mm"
            disabled={disabled}
            value={input.value || null}
            onChange={onChange}
            onClose={input.onBlur}
            onOpen={input.onFocus}
        />
    );
};
