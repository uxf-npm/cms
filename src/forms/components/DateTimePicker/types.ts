import { DateTimePickerProps as MuiDateTimePickerProps } from "@mui/x-date-pickers/DateTimePicker";
import { TextFieldProps } from "@mui/material";
import { FieldValidator } from "final-form";

export interface DateTimePickerProps<FormData extends Record<string, any>>
    extends Omit<MuiDateTimePickerProps, "name" | "required" | "onChange" | "value" | "renderInput"> {
    error?: boolean;
    name: keyof FormData;
    required?: boolean;
    requiredErrorMessage?: string;
    validate?: FieldValidator<string>[];
    variant?: TextFieldProps["variant"];
}
