import { FieldValidator } from "final-form";
import React, { ReactElement, ReactNode, useCallback } from "react";
import { useField } from "react-final-form";
import { DatePicker as UIDatePicker, DatePickerProps as UIDatePickerProps } from "../../../ui/DatePicker";
import { composeValidators } from "../../../utils/composeValidators";
import { DatePickerProps } from "./types";
import dayjs from "dayjs";

const requiredValidator =
    (errorMessage: ReactNode): FieldValidator<string> =>
    (value: string) =>
        value ? undefined : errorMessage;

export const DatePicker = <FormData extends Record<string, any>>(props: DatePickerProps<FormData>): ReactElement => {
    const {
        disabled,
        error,
        name,
        required,
        requiredErrorMessage = "Vyplňte políčko",
        validate = [],
        mask,
        ...restProps
    } = props;

    const validator = composeValidators(
        required && !disabled ? requiredValidator(requiredErrorMessage) : undefined,
        ...validate,
    );

    const { input, meta } = useField<string | null>(name as string, {
        allowNull: true,
        validate: validator,
    });

    const onChange = useCallback<UIDatePickerProps["onChange"]>(
        (date) => {
            if (date?.isValid()) {
                input.onChange(date?.format("YYYY-MM-DD"));
            } else if (!date) {
                input.onChange(null);
            }
        },
        [input],
    );

    return (
        <UIDatePicker
            clearable
            showTodayButton
            value={input.value ? dayjs(input.value) : null}
            onChange={onChange}
            disabled={disabled}
            required={required}
            error={error || (meta.touched && meta.error)}
            onBlur={input.onBlur}
            onFocus={input.onFocus}
            mask={mask ?? "__.__.____"}
            {...restProps}
        />
    );
};
