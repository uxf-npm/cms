import { DatePickerProps as UIDatePickerProps } from "../../../ui/DatePicker";
import { FieldValidator } from "final-form";

export interface DatePickerProps<FormData extends Record<string, any>>
    extends Omit<UIDatePickerProps, "onChange" | "value" | "renderInput"> {
    name: keyof FormData;
    requiredErrorMessage?: string;
    validate?: FieldValidator<string>[];
}
