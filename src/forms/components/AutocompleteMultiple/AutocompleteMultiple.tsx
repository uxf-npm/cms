import { FieldValidator } from "final-form";
import {
    AutocompleteMultiple as UIAutocompleteMultiple,
    AutocompleteMultipleOption,
} from "../../../ui/AutocompleteMultiple";
import React, { FocusEventHandler, ReactElement, ReactNode, useCallback } from "react";
import { useField } from "react-final-form";
import { AutocompleteMultipleProps } from "./types";

const requiredValidator =
    (errorMessage: ReactNode): FieldValidator<any[]> =>
    (value) =>
        (value ?? []).length > 0 ? undefined : errorMessage;

export const AutocompleteMultiple = <
    FormData extends Record<string, any>,
    T extends AutocompleteMultipleOption,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined,
>(
    props: AutocompleteMultipleProps<FormData, T, DisableClearable, FreeSolo>,
): ReactElement => {
    const {
        label,
        name,
        onBlur,
        onFocus,
        required,
        requiredErrorMessage = "Vyplňte políčko",
        TextFieldProps = {},
        ...restProps
    } = props;

    const { input, meta } = useField<T[]>(name as string, {
        // initialValue: [], //TODO proč tohle nefunguje?
        allowNull: true,
        validate: required ? requiredValidator(requiredErrorMessage) : undefined,
    });

    const onBlurHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onBlur(e);
            if (onBlur) {
                onBlur(e);
            }
        },
        [input, onBlur],
    );

    const onFocusHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onFocus(e);
            if (onFocus) {
                onFocus(e);
            }
        },
        [input, onFocus],
    );

    return (
        <UIAutocompleteMultiple
            value={input.value || []}
            onChange={(_, value) => input.onChange(value)}
            label={required ? `${label} *` : label}
            TextFieldProps={{
                ...TextFieldProps,
                helperText: meta.error && meta.touched ? meta.error : undefined,
                error: TextFieldProps.error || (meta.error && meta.touched),
            }}
            onBlur={onBlurHandler}
            onFocus={onFocusHandler}
            {...restProps}
        />
    );
};
