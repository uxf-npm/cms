import {
    AutocompleteMultipleOption,
    AutocompleteMultipleProps as UIAutocompleteMultipleProps,
} from "../../../ui/AutocompleteMultiple";
import { ReactNode } from "react";

export type AutocompleteMultipleProps<
    FormData extends Record<string, any>,
    T extends AutocompleteMultipleOption,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined,
> = Omit<UIAutocompleteMultipleProps<T, DisableClearable, FreeSolo>, "onChange" | "name" | "value" | "multiple"> & {
    name: keyof FormData;
    required?: boolean;
    requiredErrorMessage?: ReactNode;
};
