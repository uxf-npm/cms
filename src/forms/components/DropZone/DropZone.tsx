import { useDropzone } from "react-dropzone";
import React, { useState } from "react";
import { useField } from "react-final-form";
import { FileResponse } from "../../../ui/FileUploadInput";
import { container } from "../../../config";
import { resizerImageUrl } from "@uxf/core/utils/resizer";

export interface DropZoneProps {
    name: string;
}

export type FileState = FileResponse;

interface ImagePreviewProps {
    loading?: boolean;
    onRemove?: () => void;
    src?: string;
    alt?: string;
    title?: string;
}

const ImagePreview: React.FC<ImagePreviewProps> = (props) => {
    const { loading, onRemove, src, alt, title } = props;
    return (
        <div className="w-16 h-16 mr-2 mb-2 rounded-md overflow-hidden relative border border-gray-300">
            {onRemove && (
                <button
                    className="absolute right-0 top-0 bg-black hover:bg-opacity-70 bg-opacity-50 rounded-bl-md"
                    onClick={onRemove}
                >
                    <svg
                        className="w-5 h-5 text-white"
                        fill="none"
                        stroke="currentColor"
                        viewBox="0 0 24 24"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            )}
            <img alt={alt} title={title} className="h-full w-full" src={src} />
            {loading && (
                <div className="absolute top-0 left-0 right-0 bottom-0 flex items-center justify-center bg-black bg-opacity-50">
                    <svg
                        className="animate-spin h-6 w-6 text-white"
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                    >
                        <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4" />
                        <path
                            className="opacity-75"
                            fill="currentColor"
                            d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                        />
                    </svg>
                </div>
            )}
        </div>
    );
};

export const DropZone: React.FC<DropZoneProps> = (props) => {
    const { name } = props;
    const { input } = useField<FileState[]>(name, {});

    const fieldValue = input.value || [];

    const [acceptedFiles, setAcceptedFiles] = useState<File[]>([]);

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop: async (acceptedFiles) => {
            setAcceptedFiles(acceptedFiles);
            const uploadFile = container.get("api.uploadFile");
            const responses = await Promise.allSettled(acceptedFiles.map((af) => uploadFile(af)));
            const successResponses = responses
                .map((r) => (r.status === "fulfilled" ? r.value : null))
                .filter((r) => r !== null);
            setAcceptedFiles([]);
            input.onChange([...fieldValue, ...successResponses]);
        },
    });

    const onRemove = (index: number) => () => {
        // eslint-disable-next-line no-alert
        if (confirm("Opravdu chcete smazat fotografii?")) {
            const newWalue = [...fieldValue];
            newWalue.splice(index, 1);
            input.onChange(newWalue);
        }
    };

    return (
        <div>
            <div
                {...getRootProps({ className: "dropzone" })}
                className={
                    "border-dashed rounded-md border-2 py-8 bg-gray-100 hover:border-gray-300 cursor-pointer " +
                    (isDragActive ? "border-gray-300" : "border-gray-200")
                }
            >
                <input {...getInputProps()} />
                <p className="text-center text-xs text-gray-400">Drag drop some files here, or click to select files</p>
            </div>
            <div className="pt-2 flex flex-row flex-wrap">
                {fieldValue.map((file, index) => (
                    <ImagePreview
                        key={file.id}
                        src={resizerImageUrl(file, 200, 200, {}) ?? ""}
                        onRemove={onRemove(index)}
                        alt={file.name}
                        title={file.name}
                    />
                ))}
                {acceptedFiles.map((file) => (
                    <ImagePreview key={file.name} loading src={URL.createObjectURL(file)} />
                ))}
            </div>
        </div>
    );
};
