import { FormProps } from "react-final-form";

export interface ForgottenPasswordFormData {
    username: string;
}

export interface ForgottenPasswordFormProps extends FormProps<ForgottenPasswordFormData> {
    onLogin?: () => void;
}
