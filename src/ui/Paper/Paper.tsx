import React from "react";
import { PaperProps as MuiPaperProps } from "@mui/material/Paper";
import { PaperProps } from "./types";
import Box from "@mui/material/Box";

export const Paper: React.FC<PaperProps> = (props) => {
    const { ml, mt, mr, mb, ph = 2, pv = 2 } = props;
    const sx: MuiPaperProps["sx"] = {
        ml,
        mt,
        mr,
        mb,
        paddingX: ph,
        paddingY: pv,
        ...props.sx,
    };
    return (
        <Box sx={sx} {...props}>
            {props.children}
        </Box>
    );
};
