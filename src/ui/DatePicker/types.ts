import { DatePickerProps as MuiDatePickerProps } from "@mui/x-date-pickers/DatePicker";
import { TextFieldProps } from "@mui/material";
import { ReactNode } from "react";
import { Dayjs } from "dayjs";

export interface DatePickerProps extends Omit<MuiDatePickerProps<Dayjs>, "renderInput"> {
    required?: boolean;
    error?: ReactNode;
    onFocus?: () => void;
    onBlur?: () => void;
    variant?: TextFieldProps["variant"];
}
