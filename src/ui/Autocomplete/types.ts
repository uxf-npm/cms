import { TextFieldProps } from "@mui/material/TextField";
import { AutocompleteProps as MuiAutocompleteProps } from "@mui/material/Autocomplete";

export type AutocompleteOption = { id: string | number; label: string };

export type AutocompleteProps<
    T extends AutocompleteOption,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined,
> = Omit<MuiAutocompleteProps<T, false, DisableClearable, FreeSolo>, "options" | "renderInput" | "multiple"> & {
    label?: TextFieldProps["label"];
    loadOptions: (term: string) => Promise<T[]>;
    placeholder?: TextFieldProps["placeholder"];
    TextFieldProps?: Partial<TextFieldProps>;
};
