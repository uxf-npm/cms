import { ReactNode } from "react";

export interface LabelProps {
    children: ReactNode;
    className?: string;
    color?: "primary" | "secondary" | "error" | "warning" | "success" | "disabled";
    light?: boolean;
    mb?: number;
    ml?: number;
    mr?: number;
    mt?: number;
    size?: "small" | "default";
}
