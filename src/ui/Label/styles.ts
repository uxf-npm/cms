import { alpha, styled } from "@mui/material/styles";
import { LabelProps } from "./types";

export const Root: any = styled("span", { shouldForwardProp: (name) => name !== "light" })<LabelProps>(
    ({ light, mt, mb, ml, mr, size, theme }) => ({
        fontFamily: theme.typography.fontFamily,
        alignItems: "center",
        borderRadius: 4,
        display: "inline-flex",
        flexGrow: 0,
        whiteSpace: "nowrap",
        cursor: "default",
        flexShrink: 0,
        fontSize: size === "small"  ? theme.typography.pxToRem(10) : theme.typography.pxToRem(12),
        fontWeight: theme.typography.fontWeightMedium,
        height: 20,
        justifyContent: "center",
        letterSpacing: 0.5,
        minWidth: 20,
        padding: size === "small" ? theme.spacing(0, 1) : theme.spacing(0.5, 1),
        textTransform: size !== "small" ? "uppercase" : undefined,
        marginTop: mt === undefined ? undefined : theme.spacing(mt),
        marginBottom: mb === undefined ? undefined : theme.spacing(mb),
        marginLeft: ml === undefined ? undefined : theme.spacing(ml),
        marginRight: mr === undefined ? undefined : theme.spacing(mr),
        "&.primary": {
            color: light ? theme.palette.primary.main : "white",
            backgroundColor: light ? alpha(theme.palette.primary.main, 0.08) : theme.palette.primary.main,
        },
        "&.secondary": {
            color: light ? theme.palette.secondary.main : "black",
            backgroundColor: light ? alpha(theme.palette.secondary.main, 0.08) : theme.palette.secondary.main,
        },
        "&.error": {
            color: light ? theme.palette.error.main : "white",
            backgroundColor: light ? alpha(theme.palette.error.main, 0.08) : theme.palette.error.main,
        },
        "&.success": {
            color: light ? theme.palette.success.main : "white",
            backgroundColor: light ? alpha(theme.palette.success.main, 0.08) : theme.palette.success.main,
        },
        "&.warning": {
            color: light ? theme.palette.warning.main : "white",
            backgroundColor: light ? alpha(theme.palette.warning.main, 0.08) : theme.palette.warning.main,
        },
        "&.disabled": {
            color: light ? theme.palette.grey[500] : "black",
            backgroundColor: light ? alpha(theme.palette.grey[500], 0.08) : theme.palette.grey[500],
        },
    }),
);
