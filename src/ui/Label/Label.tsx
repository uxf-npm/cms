import type { FC } from "react";
import React from "react";
import { Root } from "./styles";
import { LabelProps } from "./types";

export const Label: FC<LabelProps> = (props) => {
    const { color = "secondary", children, size = "default", ...rest } = props;

    return (
        <Root className={color} size={size} {...rest}>
            {children}
        </Root>
    );
};
