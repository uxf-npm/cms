import { SelectProps as BaseSelectProps } from "@mui/material/Select/Select";

export type SelectOption<T extends string | number> = { id: T; label: string };

export type SelectProps<T extends string | number> = {
    helperText?: string;
    options: SelectOption<T>[];
} & BaseSelectProps<T>;
