import { FormHelperText, MenuItem } from "@mui/material";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import BaseSelect from "@mui/material/Select";
import React from "react";
import { SelectProps } from "./types";

export const Select = <T extends string | number>({
    helperText,
    label,
    labelId,
    options,
    value,
    variant = "outlined",
    ...restProps
}: SelectProps<T>) => (
    <FormControl fullWidth size="small">
        <InputLabel id={labelId}>{label}</InputLabel>
        <BaseSelect<T>
            size="small"
            labelId={labelId}
            label={label}
            variant={variant}
            value={value ?? ("" as any)}
            {...restProps}
        >
            {options.map((o) => (
                <MenuItem key={o.id} value={o.id}>
                    {o.label}
                </MenuItem>
            ))}
        </BaseSelect>
        {helperText && <FormHelperText>{helperText}</FormHelperText>}
    </FormControl>
);
