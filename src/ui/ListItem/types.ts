import { ListItemButtonProps } from "@mui/material";
import { SvgIconProps } from "@mui/material/SvgIcon";
import { ComponentType, ReactElement } from "react";

export interface ListItemProps<RouteList> {
    children?: ReactElement;
    icon?: ComponentType<SvgIconProps>;
    label: string;
    level?: number;
    onClick?: (event?: any) => void;
    onMouseEnter?: ListItemButtonProps["onMouseEnter"];
    onMouseLeave?: ListItemButtonProps["onMouseLeave"];
    params?: any; // TODO
    route?: keyof RouteList;
    selected?: boolean;
    href?: string;
}
