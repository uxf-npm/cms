import { TextFieldProps } from "@mui/material/TextField";
import { AutocompleteProps as MuiAutocompleteProps } from "@mui/material/Autocomplete";

export type AutocompleteMultipleOption = { id: string | number; label: string };

export type AutocompleteMultipleProps<
    T extends AutocompleteMultipleOption,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined,
> = Omit<MuiAutocompleteProps<T, true, DisableClearable, FreeSolo>, "options" | "renderInput" | "multiple"> & {
    label?: TextFieldProps["label"];
    loadOptions: (term: string) => Promise<T[]>;
    placeholder?: TextFieldProps["placeholder"];
    TextFieldProps?: Partial<TextFieldProps>;
};
