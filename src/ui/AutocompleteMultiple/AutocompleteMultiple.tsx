import CircularProgress from "@mui/material/CircularProgress";
import TextField from "@mui/material/TextField";
import MuiAutocomplete from "@mui/material/Autocomplete";
import React, { useEffect, useReducer } from "react";
import { reducer } from "./reducer";
import { AutocompleteMultipleOption, AutocompleteMultipleProps } from "./types";

export const AutocompleteMultiple = <
    T extends AutocompleteMultipleOption,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined,
>({
    label,
    loadOptions,
    placeholder,
    TextFieldProps = {},
    ...autocompleteProps
}: AutocompleteMultipleProps<T, DisableClearable, FreeSolo>): JSX.Element => {
    const [state, dispatch] = useReducer(reducer, {
        term: "",
        error: null,
        options: [],
        loading: false,
        loadingId: 1,
    });

    useEffect(() => {
        const loadingId = state.loadingId;
        loadOptions(state.term)
            .then((options) => dispatch({ type: "LOADING_DONE", loadingId, options }))
            .catch((error) => dispatch({ type: "LOADING_ERROR", loadingId, error }));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [state.loadingId]);

    return (
        <MuiAutocomplete<T, true, DisableClearable, FreeSolo>
            options={state.options}
            loading={state.loading}
            onInputChange={(_, term) => dispatch({ type: "SEARCH", term })}
            inputValue={state.term}
            size="small"
            filterOptions={(o) => o}
            noOptionsText={state.error ? "Nepodařilo se načíst data" : "Nenalezeny žádné záznamy"}
            loadingText={"Načítání\u2026"}
            fullWidth
            isOptionEqualToValue={(o, v) => o.id === v.id}
            renderOption={(props, option) => <li {...props} key={option.id}>{option.label}</li>}
            renderInput={(params) => (
                <TextField
                    placeholder={placeholder}
                    label={label}
                    variant="outlined"
                    {...params}
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <>
                                {state.loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {params.InputProps.endAdornment}
                            </>
                        ),
                    }}
                    {...TextFieldProps}
                    InputLabelProps={{ shrink: true, ...(TextFieldProps?.InputLabelProps ?? {}) }}
                />
            )}
            {...autocompleteProps}
            multiple
        />
    );
};
