import React, { useCallback, useState } from "react";
import { ClipboardButtonProps } from "./types";
import FileCopyIcon from "@mui/icons-material/FileCopy";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";


function copyTextToClipboard(textToCopy: string) {
    if (!!navigator.clipboard) {
        navigator.clipboard.writeText(textToCopy).then(
            function() {},
            function(err) {
                throw err;
            }
        );
    }

    const textArea = document.createElement("textarea");
    textArea.value = textToCopy;

    // Avoid scrolling to bottom
    textArea.style.top = "0";
    textArea.style.left = "0";
    textArea.style.position = "fixed";

    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();
    textArea.setSelectionRange(0, 99999); // for mobile devices

    document.execCommand('copy');
    document.body.removeChild(textArea);
}

export const CopyToClipboardButton: React.FC<ClipboardButtonProps> = props => {
    const { textToCopy, size = "small", color = "primary" } = props;

    const [tooltipVisible, setTooltipVisible] = useState(false);

    const copyUrl = useCallback(() => {
        try {
            copyTextToClipboard(textToCopy);
            setTooltipVisible(true);
            setTimeout(() => {
                setTooltipVisible(false);
            }, 3000);
        } catch (err) {
            console.log(err);
        }
    }, [textToCopy]);

    return (
        <Tooltip open={tooltipVisible} title="Text zkopírován" placement="bottom">
            <IconButton size={size} color={color} onClick={copyUrl}>
                <FileCopyIcon />
            </IconButton>
        </Tooltip>
    );
};