import { ReactNode } from "react";

export interface FileInputProps {
    id: string;
    name: string;
    placeholder?: string;
    disabled?: boolean;
    label: ReactNode;
    onChange: (file: File | null) => void;
    value: File | null | undefined;
}
