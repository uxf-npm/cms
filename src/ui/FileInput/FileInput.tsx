import ClearIcon from "@mui/icons-material/Clear";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";
import React from "react";
import { FileInputProps } from "./types";

export const FileInput: React.FC<FileInputProps> = (props) => {
    const { id, name, label, placeholder, value, onChange, disabled } = props;

    const handleChange = async (e: React.ChangeEvent<HTMLInputElement>): Promise<void> => {
        e.persist();

        const files = e.target.files ?? [];

        if (files.length > 1) {
            // eslint-disable-next-line no-alert
            alert("Vyberte pouze 1 soubor.");
        }

        if (files.length === 1) {
            onChange(files[0]);
        }
    };

    // TODO @vejvis - stylování (barvičky z theme?)

    return (
        <>
            <Box
                position="relative"
                display="flex"
                flexDirection="row"
                pl={1.5}
                pr={1}
                sx={{
                    borderRadius: "4px",
                    border: "1px solid #c4c4c4",
                    mb: 1,
                    height: "40px",
                    alignItems: "center",
                    "&:hover": {
                        borderColor: disabled ? undefined : "#212121",
                    },
                }}
            >
                <input type="file" id={id} name={name} style={{ display: "none" }} onChange={handleChange} />
                <Box position="absolute" top={-10} left={8} px={0.5} sx={{ backgroundColor: "white" }}>
                    <Typography fontSize="small" color="rgba(0,0,0,0.6)">
                        {label}
                    </Typography>
                </Box>
                <Typography
                    flex={1}
                    color={(theme) => (value ? undefined : theme.palette.text.disabled)}
                    textOverflow="ellipsis"
                    minWidth={0}
                    whiteSpace="nowrap"
                    overflow="hidden"
                    sx={{ lineBreak: "anywhere" }}
                    pr={1}
                >
                    {value ? value.name : placeholder}
                </Typography>
                {value ? (
                    <Tooltip title="Smazat">
                        <IconButton onClick={() => onChange(null)}>
                            <ClearIcon />
                        </IconButton>
                    </Tooltip>
                ) : (
                    <Button disabled={disabled} component="label" size="small" variant="contained" htmlFor={id}>
                        Vybrat soubor
                    </Button>
                )}
            </Box>
        </>
    );
};
