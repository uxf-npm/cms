import { ReactNode } from "react";

export interface FileResponse {
    id: number;
    name: string;
    uuid: string;
    extension: string;
    namespace: string | null;
    type?: string;
}

export interface FileUploadInputProps {
    id: string;
    name: string;
    error?: string;
    helperText?: string;
    namespace?: string;
    placeholder?: string;
    disabled?: boolean;
    label: ReactNode;
    getPreviewUrl?: (file: FileResponse) => string;
    onChange: (file: FileResponse | null) => void;
    onError?: (e: any) => void;
    value: FileResponse | null | undefined;
}
