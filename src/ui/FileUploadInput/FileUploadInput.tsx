import ClearIcon from "@mui/icons-material/Clear";
import InsertPhotoIcon from "@mui/icons-material/InsertPhoto";
import DownloadIcon from "@mui/icons-material/Download";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import Tooltip, { tooltipClasses, TooltipProps } from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";
import React, { useState } from "react";
import { FileResponse, FileUploadInputProps } from "./types";
import { container } from "../../config";
import CircularProgress from "@mui/material/CircularProgress";
import { resizerImageUrl } from "@uxf/core/utils/resizer";
import { styled } from "@mui/material/styles";

const NoMaxWidthTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
))({
    [`& .${tooltipClasses.tooltip}`]: {
        maxWidth: "none",
    },
});

const FilePreview: React.FC<{ value: FileResponse }> = (props) => {
    const { value } = props;

    if (value.type && value.type.startsWith("image/")) {
        return (
            <NoMaxWidthTooltip
                placement="left-start"
                title={
                    <img
                        alt="Náhled nahraného obrázku"
                        src={resizerImageUrl(value, 500, 500, { fit: "inside" }) ?? ""}
                    />
                }
            >
                <IconButton>
                    <InsertPhotoIcon />
                </IconButton>
            </NoMaxWidthTooltip>
        );
    }

    return (
        <Tooltip title="Stáhnout soubor">
            <IconButton
                href={`/upload/${value.namespace}/${value.uuid.charAt(0)}/${value.uuid.charAt(1)}/${value.uuid}.${
                    value.extension
                }`}
            >
                <DownloadIcon />
            </IconButton>
        </Tooltip>
    );
};

export const FileUploadInput: React.FC<FileUploadInputProps> = (props) => {
    const { id, name, label, placeholder, value, onChange, disabled, namespace, onError, error, helperText } = props;

    const [uploading, setUploading] = useState(false);

    const handleChange = async (e: React.ChangeEvent<HTMLInputElement>): Promise<void> => {
        e.persist();

        const files = e.target.files ?? [];

        if (files.length > 1) {
            // eslint-disable-next-line no-alert
            alert("Vyberte pouze 1 soubor.");
        }

        if (files.length === 1) {
            try {
                setUploading(true);
                const response = await container.get("api.uploadFile")(files[0], namespace);
                onChange(response);
            } catch (e) {
                if (onError) {
                    onError(e);
                }
            } finally {
                setUploading(false);
            }
        }
    };

    return (
        <>
            <Box
                position="relative"
                display="flex"
                flexDirection="row"
                pl={1.5}
                pr={1}
                sx={{
                    borderRadius: "4px",
                    borderWidth: "1px",
                    borderStyle: "solid",
                    borderColor: (theme) => (error ? theme.palette.error.main : "#c4c4c4"),
                    //mb: 1,
                    height: "40px",
                    alignItems: "center",
                    "&:hover": {
                        borderColor: disabled ? undefined : "#212121",
                    },
                }}
            >
                <input type="file" id={id} name={name} style={{ display: "none" }} onChange={handleChange} />
                <Box position="absolute" top={-10} left={8} px={0.5} sx={{ backgroundColor: "white" }}>
                    <Typography
                        fontSize="small"
                        color={(theme) => (error ? theme.palette.error.main : "rgba(0,0,0,0.6)")}
                    >
                        {label}
                    </Typography>
                </Box>
                <Typography
                    flex={1}
                    color={(theme) => (value ? undefined : theme.palette.text.disabled)}
                    textOverflow="ellipsis"
                    minWidth={0}
                    whiteSpace="nowrap"
                    overflow="hidden"
                    sx={{ lineBreak: "anywhere" }}
                    pr={1}
                >
                    {value ? value.name : uploading ? "Chvilku strpení prosím..." : placeholder}
                </Typography>
                {uploading && (
                    <Box px={2} display="flex" alignItems="center">
                        <CircularProgress size={24} color="secondary" />
                    </Box>
                )}
                {value ? (
                    <>
                        <FilePreview value={value} />
                        <Tooltip title="Smazat">
                            <IconButton onClick={() => onChange(null)}>
                                <ClearIcon />
                            </IconButton>
                        </Tooltip>
                    </>
                ) : (
                    <Button
                        disabled={disabled || uploading}
                        component="label"
                        size="small"
                        variant="contained"
                        htmlFor={id}
                    >
                        {uploading ? "Nahrávám" : "Vybrat soubor"}
                    </Button>
                )}
            </Box>
            {(error || helperText) && (
                <Typography
                    variant="caption"
                    color={(theme) => (error ? theme.palette.error.main : undefined)}
                    pl={1.7}
                >
                    {error || helperText}
                </Typography>
            )}
        </>
    );
};
