import React, { FC, useContext, useState } from "react";
import { CmsContextType } from "./types";

const CmsContext = React.createContext<CmsContextType>({
    sidebar: {
        isOpen: false,
        open: () => null,
        close: () => null,
    },
});

export const useCmsContext = <T extends keyof CmsContextType>(key: T): CmsContextType[T] => {
    return useContext(CmsContext)[key];
};

export const CmsProvider: FC<{children?: React.ReactNode}> = (props) => {
    const { children } = props;

    const [isOpen, setIsOpen] = useState(false);

    const value: CmsContextType = {
        sidebar: {
            isOpen,
            open: () => setIsOpen(true),
            close: () => setIsOpen(false),
        },
    };

    return <CmsContext.Provider value={value}>{children}</CmsContext.Provider>;
};
