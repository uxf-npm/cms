import React from "react";
import { flashMessagesRef } from "../../services/FlashMessagesService";
import { FlashMessagesContainer } from "./FlashMessagesContainer";

export const FlashMessagesContainerInstance: React.FC = () => <FlashMessagesContainer ref={flashMessagesRef} />;
