import IconButton from "@mui/material/IconButton";
import Slide from "@mui/material/Slide";
import Snackbar, { SnackbarProps } from "@mui/material/Snackbar";
import SnackbarContent from "@mui/material/SnackbarContent";
import CloseIcon from "@mui/icons-material/Close";
import React, { useCallback } from "react";
import { getTransitionStyles, variantIcon } from "./styles";
import { FlashMessageProps, FlashMessageVariant } from "./types";

function SlideTransition(props: any) {
    return <Slide {...props} direction="right" />;
}

const ANCHOR_ORIGIN: SnackbarProps["anchorOrigin"] = {
    vertical: "bottom",
    horizontal: "left",
};

export const FlashMessage: React.FC<FlashMessageProps> = (props) => {
    const { config, offset, closing, onClose } = props;
    const variant: FlashMessageVariant = config.variant ?? "info";
    const Icon = variantIcon[variant];

    const handleClose = useCallback(() => {
        onClose(config);
    }, [onClose, config]);

    return (
        <Snackbar
            TransitionComponent={SlideTransition}
            anchorOrigin={ANCHOR_ORIGIN}
            autoHideDuration={config.dismissTimeout ?? 6000}
            onClose={(_, reason) => (reason !== "clickaway" ? handleClose() : null)}
            open={!closing}
            style={getTransitionStyles(offset)}
        >
            <SnackbarContent
                className={variant}
                aria-describedby="client-snackbar"
                message={
                    <span style={{ display: "flex", alignItems: "center" }}>
                        <Icon sx={{ fontSize: 20, opacity: 0.9, mr: 2 }} />
                        {config.body}
                    </span>
                }
                action={
                    <IconButton key="close" aria-label="Close" color="inherit" onClick={handleClose} size="large">
                        <CloseIcon sx={{ fontSize: 20 }} />
                    </IconButton>
                }
            />
        </Snackbar>
    );
};
