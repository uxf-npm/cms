import { css } from "@emotion/react";
import { amber, green } from "@mui/material/colors";
import { styled } from "@mui/material/styles";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import ErrorIcon from "@mui/icons-material/Error";
import InfoIcon from "@mui/icons-material/Info";
import WarningIcon from "@mui/icons-material/Warning";
import { FlashMessageVariant } from "./types";
import { CSSProperties } from "react";

export const variantIcon: { [variant in FlashMessageVariant]: any } = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon,
};

export const Root = styled("div")(
    ({ theme }) => css`
        .MuiSnackbarContent-root {
            &.success {
                background-color: ${green[600]};
            }

            &.error {
                background-color: ${theme.palette.error.dark};
            }

            &.info {
                background-color: ${theme.palette.primary.dark};
            }

            &.warning {
                background-color: ${amber[700]};
            }
        }
    `,
);

export const TRANSITION_DELAY = 150;
export const TRANSITION_DOWN_DURATION = 200;

export const getTransitionStyles = (offset: number): Partial<CSSProperties> => ({
    bottom: offset,
    transition: `all ${TRANSITION_DOWN_DURATION}ms ease-in-out ${TRANSITION_DELAY}ms`,
});
