import { AxiosInstance, AxiosRequestConfig, Method } from "axios";
import { makeUseAxios, ResponseValues } from "axios-hooks";
import { config as cmsConfig } from "../../config";
import { useCallback } from "react";
import { RequestConfig, Execute } from "./types";

export const createUseAxiosRequest = (axiosInstance: AxiosInstance, requestConfig: AxiosRequestConfig) => {
    const useAxios = makeUseAxios({
        axios: axiosInstance,
        defaultOptions: {
            ssr: false,
        },
    });

    return <
        Response extends {},
        RequestBody extends {} | null,
        RequestQuery extends {} | null,
        RequestPath extends {} | null,
    >(
        url: string,
        method: Method,
        config: RequestConfig<RequestBody, RequestQuery, RequestPath>,
    ): [ResponseValues<Response, any>, Execute<Response, RequestBody, RequestQuery, RequestPath>] => {
        // TODO Vejvis!!! nějaká demence s typem configu
        const { body, query } = config as any;
        const [result, executeHandler] = useAxios<Response>(
            {
                baseURL: cmsConfig.get("api-url"),
                ...requestConfig,
                url,
                method,
                params: query,
                data: body,
            },
            { useCache: false }, //TODO tohle by se asi mělo vyřešit nějak civilizovanějš
        );

        const execute = useCallback<Execute<Response, RequestBody, RequestQuery, RequestPath>>(
            (c) =>
                executeHandler({
                    url,
                    method,
                    // TODO Vejvis!!! nějaká demence s typem configu
                    params: (c as any).query,
                    data: (c as any).body,
                }),
            [url, method, executeHandler],
        );

        return [result, execute];
    };
};
