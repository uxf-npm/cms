import { ReactNode } from "react";
import { MenuItem } from "../Menu/model";

export type LayoutProps = SidebarProps & {
    children?: ReactNode;
    Breadcrumbs?: ReactNode;
    header?: ReactNode;
    loading?: boolean;
    Logo?: ReactNode;
    MessageBar?: ReactNode;
    subtitle?: ReactNode;
    title?: string;
    ToolbarRight?: ReactNode;
    TitleRightContainer?: ReactNode;
};

export interface AppBarProps {
    Logo?: ReactNode;
    ToolbarRight?: ReactNode;
}

export interface SidebarProps {
    Menu?: ReactNode;
    UserMenu?: ReactNode;
    menuConfiguration?: MenuItem[];
}
