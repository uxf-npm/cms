import CircularProgress from "@mui/material/CircularProgress";
import CssBaseline from "@mui/material/CssBaseline";
import MuiToolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Head from "next/head";
import React, { FC } from "react";
import { config } from "../../config";
import { AppBar } from "./AppBar";
import { Sidebar } from "./Sidebar";
import { LayoutHeader, LayoutRoot } from "./styles";
import { LayoutProps } from "./types";

export const Layout: FC<LayoutProps> = (props) => {
    const {
        children,
        title,
        Logo,
        Menu,
        menuConfiguration,
        UserMenu,
        header,
        Breadcrumbs,
        ToolbarRight,
        MessageBar,
        loading,
        subtitle,
        TitleRightContainer,
    } = props;

    return (
        <LayoutRoot>
            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta charSet="utf-8" />
                <title>{`${title} | ${config.get("app-name") ?? ""}`}</title>
            </Head>
            <CssBaseline />
            <AppBar Logo={Logo} ToolbarRight={ToolbarRight} />
            <div className="flex flex-row w-full">
                <Sidebar menuConfiguration={menuConfiguration} Menu={Menu} UserMenu={UserMenu} />
                <main className="sm:w-[calc(100%-240px)]">
                    {loading ? (
                        <div className="flex flex-col items-center justify-center h-96">
                            <div className="text-center">
                                <CircularProgress />
                                <Typography mt={1}>Načítám&hellip;</Typography>
                            </div>
                        </div>
                    ) : (
                        <>
                            <MuiToolbar variant="dense" />
                            <LayoutHeader hasHeader={!!header} sx={{ p: 3 }}>
                                {MessageBar}
                                <div style={{ fontSize: 10 }}>{Breadcrumbs}</div>
                                <div className="flex flex-row items-center">
                                    <Typography
                                        flex={1}
                                        variant="h4"
                                        component="h2"
                                        color={header ? "#fff" : undefined}
                                    >
                                        {title}
                                    </Typography>
                                    {TitleRightContainer}
                                </div>
                                {subtitle}
                                {header}
                            </LayoutHeader>
                            <div className="p-3">{children}</div>
                        </>
                    )}
                </main>
            </div>
        </LayoutRoot>
    );
};
