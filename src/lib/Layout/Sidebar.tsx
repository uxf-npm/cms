import Drawer from "@mui/material/Drawer";
import MuiToolbar from "@mui/material/Toolbar";
import React, { FC } from "react";
import { Menu } from "../Menu";
import { SidebarProps } from "./types";
import { useCmsContext } from "../CmsProvider";
import { drawerWidth } from "./styles";

export const Sidebar: FC<SidebarProps> = (props) => {
    const { Menu: MenuComponent, menuConfiguration, UserMenu } = props;
    const { isOpen, close } = useCmsContext("sidebar");

    const drawerContent = (
        <>
            <MuiToolbar variant="dense" />
            {MenuComponent ?? (menuConfiguration ? <Menu menu={menuConfiguration} /> : null)}
            {UserMenu}
        </>
    );

    const container = typeof window !== undefined ? () => window.document.body : undefined;

    return (
        <>
            <Drawer
                container={container}
                variant="temporary"
                open={isOpen}
                onClose={close}
                ModalProps={{
                    keepMounted: true, // Better open performance on mobile.
                }}
                sx={{
                    display: { xs: "block", sm: "none" },
                    "& .MuiDrawer-paper": {
                        boxSizing: "border-box",
                        width: drawerWidth,
                    },
                }}
            >
                {drawerContent}
            </Drawer>
            <Drawer
                variant="permanent"
                sx={{
                    flexShrink: 0,
                    width: drawerWidth,
                    display: { xs: "none", sm: "block" },
                    "& .MuiDrawer-paper": {
                        boxSizing: "border-box",
                        width: drawerWidth,
                    },
                }}
                open
            >
                {drawerContent}
            </Drawer>
        </>
    );
};
