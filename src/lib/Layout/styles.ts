import { css } from "@emotion/react";
import { styled } from "@mui/material/styles";

export const drawerWidth = 240;

export const LayoutRoot = styled("div")`
    display: flex;
`;

export const LayoutHeader = styled("div", { shouldForwardProp: (propName) => propName !== "hasHeader" })<{
    hasHeader?: boolean;
}>(
    ({ theme, hasHeader }) =>
        hasHeader &&
        css`
            background-color: ${theme.palette.primary.main};
            color: #fff;
        `,
);
