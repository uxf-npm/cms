import { ReactNode } from "react";

export interface LoginLayoutProps {
    hideTitle?: boolean;
    title: ReactNode;
    pageTitle?: string;
    Logo?: ReactNode;
    children?: ReactNode;
}
