import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import { visuallyHidden } from "@mui/utils";
import Head from "next/head";
import React from "react";
import { LoginLayoutHeader, LoginLayoutPaper, LoginLayoutRoot } from "./styles";
import { LoginLayoutProps } from "./types";

export const LoginLayout: React.FC<LoginLayoutProps> = (props) => {
    const { hideTitle, title, pageTitle, children, Logo } = props;

    return (
        <LoginLayoutRoot>
            <Head>
                <title>{pageTitle || ""}</title>
            </Head>
            <CssBaseline />
            <LoginLayoutPaper variant="outlined">
                <LoginLayoutHeader>
                    {Logo}
                    {title && (
                        <Typography
                            component="h1"
                            sx={hideTitle ? visuallyHidden : { mt: Logo ? 2 : undefined }}
                            variant="h5"
                        >
                            {title}
                        </Typography>
                    )}
                </LoginLayoutHeader>
                {children}
            </LoginLayoutPaper>
        </LoginLayoutRoot>
    );
};
