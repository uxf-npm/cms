import { Resolver } from "./types";

const exactMatch = (): Resolver => (menuItem) => (_, activeRoute) => {
    return activeRoute === menuItem.route;
};

const exactMatchWithParams = (): Resolver => (menuItem) => (router, activeRoute) => {
    const isQueryEqual = JSON.stringify(router.query) === JSON.stringify(menuItem.routeParams);
    return activeRoute === menuItem.route && isQueryEqual;
};

const anyOf =
    (routeNames: string[]): Resolver =>
    (menuItem) =>
    (_, activeRoute) => {
        if (activeRoute === menuItem.route) {
            return true;
        }

        return routeNames.some((routeName) => routeName === activeRoute);
    };

const tableLink = (): Resolver => (menuItem) => (router, activeRoute) => {
    const isQueryEqual = router.query.entityAlias === menuItem.routeParams?.entityAlias;

    if (activeRoute === menuItem.route && isQueryEqual) {
        return true;
    }

    return activeRoute === "cms/table-form" && isQueryEqual;
};

export const ActiveItemResolver = {
    anyOf,
    exactMatch,
    exactMatchWithParams,
    tableLink,
};
