import Box from "@mui/material/Box";
import List from "@mui/material/List";
import Popover from "@mui/material/Popover";
import React, { FC, MouseEventHandler, useCallback, useEffect, useRef, useState } from "react";
import { container } from "../../../config";
import { ListItem } from "../../../ui/ListItem";
import { MenuItem as UIMenuItem } from "../../../ui/MenuItem";
import { MenuItem as Model } from "../model";
import { useRouter } from "next/router";

export interface MenuItemProps {
    hasActiveChildren?: boolean;
    idPrefix?: string;
    isActive?: boolean;
    item: Model;
    level?: number;
    offset?: number;
}

export const MenuItem: FC<MenuItemProps> = (props) => {
    const { item } = props;

    const listRef = useRef<HTMLUListElement>(null);

    const router = useRouter();
    const activeRoute = container.get("useActiveRoute")();

    const isActive = item.isActive(router, activeRoute);
    const hasActiveChildren = item.hasActiveChildren(router, activeRoute);

    const useLoggedUser = container.get("useLoggedUser");
    const loggedUser = useLoggedUser();

    const [anchorEl, setAnchorEl] = useState<HTMLDivElement | null>(null);

    const onMouseEnter = useCallback<MouseEventHandler<HTMLDivElement>>((e) => {
        setAnchorEl(e.currentTarget);
    }, []);

    const onMouseLeave = useCallback(() => {
        setAnchorEl(null);
    }, []);

    useEffect(() => {
        function closeHandler(e: MouseEvent) {
            const list = listRef.current;
            if (list && e.target instanceof Element && (e.target !== list || !list.contains(e.target))) {
                setAnchorEl(null);
            }
        }
        document.addEventListener("click", closeHandler);
        return () => {
            document.removeEventListener("click", closeHandler);
        };
    }, []);

    if (!item.isVisible(loggedUser) || (item.children.length !== 0 && !item.hasVisibleChildren(loggedUser))) {
        return null;
    }

    const visibleChildren = item.children.filter((i) => i.isVisible(loggedUser));
    const firstVisibleChild = visibleChildren[0] ?? undefined;

    return (
        <>
            <Box onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
                <ListItem
                    label={item.title}
                    icon={item.icon}
                    href={item.url}
                    route={item.route ?? firstVisibleChild?.route}
                    params={item.routeParams ?? (firstVisibleChild ? firstVisibleChild.routeParams : null)}
                    selected={isActive}
                />
                {!isActive && !hasActiveChildren && item.children.length > 0 && (
                    <Popover
                        PaperProps={{
                            sx: {
                                ml: 1,
                                pointerEvents: "auto",
                            },
                        }}
                        anchorEl={anchorEl}
                        anchorOrigin={{
                            vertical: "top",
                            horizontal: "right",
                        }}
                        disableScrollLock
                        hideBackdrop
                        onClose={onMouseLeave}
                        open={!!anchorEl}
                        sx={{
                            pointerEvents: "none",
                        }}
                        transformOrigin={{
                            vertical: "top",
                            horizontal: "left",
                        }}
                    >
                        <List ref={listRef}>
                            {visibleChildren.map((i) => (
                                <UIMenuItem
                                    key={i.title}
                                    label={i.title}
                                    route={i.route}
                                    params={i.routeParams}
                                    icon={i.icon}
                                />
                            ))}
                        </List>
                    </Popover>
                )}
            </Box>
            {(isActive || hasActiveChildren) && item.children.length > 0 && (
                <List sx={{ py: 0 }}>
                    {visibleChildren.map((i) => {
                        const isSel = i.isActive(router, activeRoute);

                        return (
                            <ListItem
                                key={i.title}
                                label={i.title}
                                href={i.url}
                                icon={i.icon}
                                route={i.route}
                                params={i.routeParams}
                                level={1}
                                selected={isSel}
                            />
                        );
                    })}
                </List>
            )}
        </>
    );
};
