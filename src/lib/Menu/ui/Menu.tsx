import List from "@mui/material/List";
import React, { FC } from "react";
import { MenuItem as Model } from "../model";
import { MenuItem } from "./MenuItem";

export interface MenuProps {
    menu: Model[];
}

export const Menu: FC<MenuProps> = (props) => {
    const { menu } = props;

    return (
        <List>
            {menu.map((item) => (
                <MenuItem item={item} key={item.title} />
            ))}
        </List>
    );
};
