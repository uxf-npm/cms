import { ComponentType } from "react";
import { SvgIconProps } from "@mui/material/SvgIcon";
import { ActiveItemResolver } from "./activeItemResolver";
import { MenuItem } from "./model";

export function createSection(title: string, icon?: ComponentType<SvgIconProps>, children?: MenuItem[]): MenuItem {
    return MenuItem.create(title).setIcon(icon).setChildren(children);
}

export function createLink(
    title: string,
    routeName: string,
    routeParams?: any,
    icon?: ComponentType<SvgIconProps>,
): MenuItem {
    return MenuItem.create(title)
        .setRoute(routeName, routeParams)
        .setIcon(icon)
        .setActiveResolver(ActiveItemResolver.anyOf([routeName]));
}

export function createExternalLink(title: string, href: string, icon?: ComponentType<SvgIconProps>): MenuItem {
    return MenuItem.create(title).setIcon(icon).setUrl(href);
}

export function createTableLink(title: string, entityAlias: string, icon?: ComponentType<SvgIconProps>): MenuItem {
    return MenuItem.create(title)
        .setRoute("cms/table", { entityAlias })
        .setIcon(icon)
        .setActiveResolver(ActiveItemResolver.tableLink());
}
