import { MenuItem } from "./model";
import { NextRouter } from "next/router";

export type RouterResolver = (router: NextRouter, activeRoute: string) => boolean;
export type Resolver = (item: MenuItem) => RouterResolver;

export type MenuConfiguration = MenuItem[];
