import { SvgIconProps } from "@mui/material/SvgIcon";
import { ComponentType } from "react";
import { LoggedUser } from "../../config/Container";
import { Resolver, RouterResolver } from "./types";
import { NextRouter } from "next/router";

export class MenuItem {
    private _title: string;
    private _children: MenuItem[];
    private _url?: string;
    private _route?: string;
    private _routeParams?: any;
    private _icon?: ComponentType<SvgIconProps>;
    private _activeItemResolver?: RouterResolver;
    private _allowedRoles?: string[];

    public static create(title: string): MenuItem {
        return new MenuItem(title);
    }

    constructor(title: string) {
        this._title = title;
        this._children = [];
    }

    public setIcon(icon?: ComponentType<SvgIconProps>): MenuItem {
        this._icon = icon;
        return this;
    }

    public setChildren(children?: MenuItem[]): MenuItem {
        this._children = children ?? [];
        return this;
    }

    public setRoute(route: string, routeParams?: object): MenuItem {
        this._route = route;
        this._routeParams = routeParams;
        return this;
    }

    public setUrl(url: string): MenuItem {
        this._url = url;
        return this;
    }

    public setActiveResolver(resolver: Resolver | null): MenuItem {
        this._activeItemResolver = resolver ? resolver(this) : undefined;
        return this;
    }

    public setAllowedRoles(roles: string[]): MenuItem {
        this._allowedRoles = roles;
        return this;
    }

    public isActive(router: NextRouter, activeRoute: string): boolean {
        return !!(this._activeItemResolver && this._activeItemResolver(router, activeRoute));
    }

    public hasActiveChildren(router: NextRouter, activeRoute: string): boolean {
        return this.children.some((item) => item.isActive(router, activeRoute));
    }

    public isVisible(loggedUser: LoggedUser): boolean {
        if (this._allowedRoles === undefined) {
            return true;
        }

        for (const allowedRole of this._allowedRoles) {
            if (loggedUser.roles.includes(allowedRole)) {
                return true;
            }
        }

        return false;
    }

    public hasVisibleChildren(loggedUser: LoggedUser): boolean {
        return this.children.some((item) => item.isVisible(loggedUser));
    }

    get title(): string {
        return this._title;
    }

    get children(): MenuItem[] {
        return this._children;
    }

    get url(): string | undefined {
        return this._url;
    }

    get route(): string | undefined {
        return this._route;
    }

    get routeParams(): any {
        return this._routeParams;
    }

    get icon(): React.ComponentType<SvgIconProps> | undefined {
        return this._icon;
    }

    get allowedRoles(): string[] | undefined {
        return this._allowedRoles;
    }
}
