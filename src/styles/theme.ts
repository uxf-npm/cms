import { createTheme, ThemeOptions } from "@mui/material/styles";

export const defaultTypography: ThemeOptions["typography"] = {
    h1: { fontSize: 34, fontWeight: 500 },
    h2: { fontSize: 24, fontWeight: 500 },
    h3: { fontSize: 18, fontWeight: 500 },
    h4: { fontSize: 16, fontWeight: 500 },
};

export const theme = createTheme({
    palette: {
        primary: {
            main: "#5d486e",
        },
        secondary: {
            main: "#45cedb",
        },
        error: {
            main: "#ed1c24",
        },
    },
    typography: {
        fontFamily: [
            "LatoCustom",
            "-apple-system",
            "BlinkMacSystemFont",
            '"Segoe UI"',
            "Roboto",
            '"Helvetica Neue"',
            "Arial",
            "sans-serif",
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(","),
        ...defaultTypography,
    },
});
