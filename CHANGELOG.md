# Changelog

## v3.0.0-beta

- removed peerDependency `@mui/lab`
- added peerDependency `@mui/x-date-pickers@^5.0.0-alpha.1`