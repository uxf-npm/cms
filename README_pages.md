# @uxf/cms/pages

[<< README.md](./README.md)

## ContentBuilder

### ContentBuilderPage

```tsx
import {ContentBuilderPage} from "@uxf/cms/lab/ContentBuilderPage";
import {Wysiwyg, WysiwygContent} from "@uxf/cms/lab/ContentBuilderPage/content/Wysiwyg"
import {mapFormDataToRequest} from "./mapper";

type Contents = WysiwygContent | ...;

// basic usage
export default ContentBuilderPage<Contents>({
    contentComponents: [Wysiwyg],
    ui: {
        Layout: (props) => (
            <Layout menuConfiguration={menu} title="Nový článek">
                {props.children}
            </Layout>
        ),
    },
});

// full usage
export default ContentBuilderPage<Contents>({
    type: "BLOG", // use type or allowedTypes, don't use both
    allowedTypes: [{id: "BLOG", label: "Článek"}, {id: "...", label: "..."}],
    contentComponents: [Wysiwyg],
    initialValues: async (ctx) => {
        return loadContent(ctx.query.id); // implement loadContent function 
    },
    onSubmit: async (id, values) => {
        return id
            ? updateContent(id, mapFormDataToRequest(values))
            : createContent(mapFormDataToRequest(values));
    },
    onSubmitSuccess: async (id, values, response) => {
        // implement success flash message
    },
    ui: {
        Layout: (props) => (
            <Layout menuConfiguration={menu} title="Nový článek">
                {props.children}
            </Layout>
        ),
    },
    allowedRoles: ['ROLE_ADMIN'],
    // customize form - hide unwanted input fields
    hide: {
        seo: {
            ogDescription: true,
        },
    },
});
```

### ContentField
```tsx
import { Wysiwyg } from "@uxf/cms/lab/ContentBuilder/content/Wysiwyg"

// default configuration
const CONTENT_COMPONENTS = [Wysiwyg, ...];

// or custom configuration of Wysiwyg
const CONTENT_COMPONENTS = [
    Wysiwyg.create({
        editorProps: {},
    }),
    ...
];
  
const CustomForm: React.FC = () => {
    return (
        <Form>
            <ContentField contentComponents={CONTENT_COMPONENTS} name="content"/>
        </Form>
    );
}
```

### Jak přidat nový typ obsahu?

```tsx
import { ContentHeader, Content } from "@uxf/cms/pages/ContentBuilderPage";
import { TextInput } from "@uxf/cms/forms/components/TextInput";

export type WysiwygContent = Content<"wysiwyg", any>;

export const Wysiwyg: ContentComponent<WysiwygContent> = (props) => {
    const { name } = props;

    return (
        <Paper>
            <ContentHeader {...props} />
            <TextInput name="text" label="Wysiwyg" />
        </Paper>
    );
};

Wysiwyg.getConfig = () => ({
    type: "wysiwyg",
    label: "Strukturovaný text",
});
```

## ForgottenPasswordPage

```tsx
import { ForgottenPasswordPage } from "@uxf/cms/pages/ForgottenPasswordPage";

const Page = ForgottenPasswordPage({
    title: "Zapomenuté heslo",
    onSubmit: (username) => {
        /* implement me */
        return Promise.resolve();
    },
});
```

## FormPage

#### Initial values

- `initialValues: ((ctx) => Promise<T>) | T`
- `entityId: ((ctx) => Promise<number>) | number`

#### Security

- `allowedRoles: string[]`

#### Save handlers

- `onSave?: (data: T, entityAlias: string) => Promise<T>`
- `onSaveDone?: (data: T, entityAlias: string) => void`

```tsx
import { FormPage } from "@uxf/cms/pages/FormPage";

export default FormPage({
    entityAlias: "user",
    initialValues: async (ctx) => ({
        text: "text value"
    })
});
```

## GridPage
Používá balíček [@uxf/data-grid 4.0.0-beta](https://www.npmjs.com/package/@uxf/data-grid/v/4.0.0-beta.8) a TailwindCSS

## LoginPage

```tsx
import { LoginPage } from "@uxf/cms/pages/LoginPage";

export default LoginPage({
    pageTitle: "Přihlášení | UXFans",
    onLoginDone: (response, redirectUrl) => {
        console.log("Login done!");
        return Promise.resolve();
    },
});
```