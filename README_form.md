# @uxf/cms/form

[<< README.md](./README.md)

### DropZone

Používá upload funkci z `Container.get("api.uploadFile")`.

```tsx
import { DropZone } from "@uxf/cms/forms/components/DropZone";

<DropZone name="files"/>
```

### FileUploadInput
Po vybrání souboru hned nahrává na BE a vrací FileResponse

Používá upload funkci z `Container.get("api.uploadFile")`.

```tsx
import { FileUploadInput } from "@uxf/cms/forms/components/FileUploadInput";

<FileUploadInput 
    id="file-upload-input-id" 
    name="fileUploadInput" 
    label="..." 
    placeholder="..."
    namespace="upload"
    required />
```