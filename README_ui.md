# @uxf/cms/ui

[<< README.md](./README.md)

## UI

### Autocomplete
// TODO

### Button
```tsx
import { Button } from "@uxf/cms/ui/Button";

<Button loading={true}>title</Button>
```

### CopyToClipboard
```tsx
import { CopyToClipboard } from "@uxf/cms/ui/CopyToClipboard";

<CopyToClipboard value="https://" label="Zkopírovat url adresu" disabled />
```

### CopyToClipboardButton
```tsx
import { CopyToClipboardButton } from "@uxf/cms/ui/CopyToClipboardButton";

<CopyToClipboardButton textToCopy="Text ke zkopírování"/>
```

### FileInput
nativní fileInput bez uploadu (přijímá a vrací object typu File)

```tsx
import { FileInput } from "@uxf/cms/ui/FileInput";

<FileInput id="file-input-id" name="fileInput" label="..." placeholder="..." value={value} onChange={setValue} />
```

### FileUploadInput
Po vybrání souboru hned nahrává na BE a vrací FileResponse

Nutné registrovat upload funkci.

```tsx
import {container } from "@uxf/cms/config";

container
    .register("api.uploadFile", async (file, namespace) => {
        // TODO
    })
```

```tsx
import { FileUploadInput } from "@uxf/cms/ui/FileUploadInput";

<FileUploadInput 
    id="file-upload-input-id" 
    name="fileUploadInput" 
    label="..." 
    placeholder="..." 
    value={value} 
    onChange={setValue}
    namespace="upload" />
```


### Label
```tsx
import { Label } from "@uxf/cms/ui/Label";

<Label color="success">SUCCESS</Label>
```

### Paper
```tsx
import { Paper } from "@uxf/cms/ui/Paper";

<Paper mr={1} ml={2} mt={3} mb={4} ph={1} pv={2}>content</Paper>
```

### Select
// TODO