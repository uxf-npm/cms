# @uxf/cms
[![npm](https://img.shields.io/npm/v/@uxf/cms)](https://www.npmjs.com/package/@uxf/cms)
[![size](https://img.shields.io/bundlephobia/min/@uxf/cms)](https://www.npmjs.com/package/@uxf/cms)
[![quality](https://img.shields.io/npms-io/quality-score/@uxf/cms)](https://www.npmjs.com/package/@uxf/cms)
[![license](https://img.shields.io/npm/l/@uxf/cms)](https://www.npmjs.com/package/@uxf/cms)

[Changelog](./CHANGELOG.md)

## Changelog
- **v4** - přidání tailwindu
- **v5** - závislost na @uxf/router v3 (nepředává se do containeru celý router ale pouze funkce `route` a `useActiveRoute`)

## Rozdělení balíčku

- [@uxf/cms/ui](./README_ui.md) - UI komponenty
- [@uxf/cms/form](./README_form.md) - Formulářové prvky
- [@uxf/cms/pages](./README_pages.md) - NextJS stránky
- [@uxf/cms/lab](./README_lab.md) - Komponenty které ještě nejsou odladěné na stabilní verzi

## Instalace

Od verze 4.0 balíček používá Tailwind CSS

```shell
yarn add @uxf/cms dayjs
```

Do souboru `tailwind.config.js` přidat následující řádky

```js
/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./node_modules/@uxf/cms/**/*.js"
    ],
    plugins: [],
};
```

## Použití balíčku

Konfigurace balíčku se prvádí v souboru `_app.tsx`.

```tsx
// _app.tsx
import { locale } from "dayjs";
import "dayjs/locale/cs";
import { config, container } from "@uxf/cms/config";

cmsConfig.set("api-url", "== api url ==");

container
    .register("router", Router<any>)
    .register("service.error", ErrorService)
    .register("service.notification", FlashMessagesService) // default value
    .register("useLoggedUser", UseLoggedUser)
    .register("api.getLoggedUser", GetLoggedUser)
    .register("api.getUserConfig", GetUserConfig)
    .register("api.uploadFile", (file, namespace) => TODO); // default value

locale("cs")
```

## Api
- cookie je předávána automaticky z kontextu

```tsx
import {...} from  "@uxf/cms/api";
```

- `getLoggedUser`
- `getEntityMetaSchemas`
- `getFormSchema`
- `getFormValues`
- `login`
- `autocompleteRows`
- `saveFormValues`
- `userConfigSave`
- `userConfigGetAll`
- `userConfigGet`
- `uploadFile`

## FlashMessages

```tsx
// pages/_app.tsx

import { FlashMessagesContainerInstance } from "@uxf/cms/lib/FlashMessages";

function App(props: any) {
    const { Component, pageProps } = props;

    return (
        <>
            <Component {...pageProps} />
            <FlashMessagesContainerInstance />
        </>
    );
}

export default App;
```

```tsx
// pages/index.tsx

import { FlashMessagesService } from "@uxf/cms/services/FlashMessagesService";

function IndexPage(props) {
    return <Button onClick={() => FlashMessagesService.success("Hotovo.")}>Klikni</Button>;
}

export default IndexPage;
```

## Jak vyvíjet?
```bash
git clone git@gitlab.com:uxf-npm/cms.git
cd ./cms
npm install
make dev
```

## Vydání balíčku:

Balíček se vydá po mergi do `master` nebo `beta` branche.

K publikování nových verzí do npm je opužit balíček [semantic-release](https://github.com/semantic-release/semantic-release) .

### Jak má vypadat commit message?

https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines

| Commit message                                                                                                                                                                                   | Release type               |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------|
| `fix(pencil): stop graphite breaking when too much pressure applied`                                                                                                                             | Patch Release              |
| `feat(pencil): add 'graphiteWidth' option`                                                                                                                                                       | ~~Minor~~ Feature Release  |
| `perf(pencil): remove graphiteWidth option`<br><br>`BREAKING CHANGE: The graphiteWidth option has been removed.`<br>`The default graphite width of 10mm is always used for performance reasons.` | ~~Major~~ Breaking Release |
