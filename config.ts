import { createExternalLink, createLink, createSection, MenuConfiguration } from "./src/lib/Menu";
import LoginIcon from "@mui/icons-material/Person";
import FormIcon from "@mui/icons-material/FormatListBulleted";
import PagesIcon from "@mui/icons-material/MenuBook";
import HomeIcon from "@mui/icons-material/Home";
import LockIcon from "@mui/icons-material/Lock";
import PersonAdd from "@mui/icons-material/PersonAdd";
import LoadingIcon from "@mui/icons-material/Loop";
import { Reply } from "@mui/icons-material";

export const menu: MenuConfiguration = [
    createExternalLink("Přejít na web", "/", Reply),
    createLink("Domů", "index", null, HomeIcon),
    createSection("Formuláře", FormIcon, [
        createLink("Komponenty", "form", null, FormIcon),
        createLink("Pozvání uživatele", "forms/user-invitation", null, PersonAdd),
    ]),
    createSection("Stránky", PagesIcon, [
        createLink("Přihlášení", "pages/login-page", null, LoginIcon),
        createLink("Formulář", "pages/form-page", null, FormIcon),
        createLink("Zapomenuté heslo", "pages/forgotten-password-page", null, LockIcon),
        createLink("Obnova hesla", "pages/renew-password-page", null, LockIcon),
        createLink("Grid page", "pages/grid-page", null, LockIcon),
        createLink("Content builder page", "pages/content-builder-page", null, LockIcon),
    ]),
    createLink("Full page loading", "loader", null, LoadingIcon),
];
